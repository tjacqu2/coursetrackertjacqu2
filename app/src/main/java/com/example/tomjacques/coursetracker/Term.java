package com.example.tomjacques.coursetracker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomjacques on 1/20/18.
 */

public class Term {

    private int termId;
    private int termNumber;
    private String termName;
    private String termStart;
    private String termEnd;
    private final List<Course> associatedCourses = new ArrayList<>();
    private static List<Term> termList = new ArrayList<>();

    public Term() {

    }

    public Term(int termId, int termNumber, String termName, String termStart, String termEnd, Course associatedCourses) {
        this.termId = termId;
        this.termNumber = termNumber;
        this.termName = termName;
        this.termStart = termStart;
        this.termEnd = termEnd;
        this.associatedCourses.add(associatedCourses);
    }

    public int getTermId() {
        return termId;
    }

    public void setTermId(int termId) {
        this.termId = termId;
    }

    public int getTermNumber() {
        return termNumber;
    }

    public void setTermNumber(int termNumber) {
        this.termNumber = termNumber;
    }

    public String getTermStart() {
        return termStart;
    }

    public void setTermStart(String termStart) {
        this.termStart = termStart;
    }

    public String getTermEnd() {
        return termEnd;
    }

    public void setTermEnd(String termEnd) {
        this.termEnd = termEnd;
    }

    public List<Course> getAssociatedCourses() {
        return associatedCourses;
    }

    public static List<Term> getTermList() {
        return termList;
    }

    public static void setTermList(List<Term> termList) {
        Term.termList = termList;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }
}
