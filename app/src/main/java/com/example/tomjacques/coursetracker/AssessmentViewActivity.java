package com.example.tomjacques.coursetracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by tomjacques on 1/13/18.
 */

public class AssessmentViewActivity extends AppCompatActivity {
    public static boolean fromAssessmentView = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //text fields that will be filled with current assessment data
        TextView assessAlert = findViewById(R.id.assessview_goal_date_title);
        TextView courseNum = findViewById(R.id.assessview_course_num);
        TextView courseName = findViewById(R.id.assessview_course_name);
        TextView assessName = findViewById(R.id.assessview_assess_name);
        TextView assessType = findViewById(R.id.assessview_assess_type);
        TextView questions = findViewById(R.id.assessview_questions);
        TextView passFail = findViewById(R.id.assessview_pass_fail);
        TextView myScore = findViewById(R.id.assessview_myScore);
        TextView dueDate = findViewById(R.id.assessview_due_date);
        TextView goalDate = findViewById(R.id.assessview_goal_date);
        //populate assessment view
        for (Assessment a : Assessment.getAssessmentList()) {
            if (AssessmentListAdapter.assessIdMatch == a.getAssessId()) {//match id from screen to list/db
                assessName.setText(a.getAssessName());
                assessType.setText(a.getAssessType());
                questions.setText(Integer.toString(a.getQuestions()));
                passFail.setText(a.getPassFailCriteria());
                myScore.setText(a.getMyScore());

                String reformattedDateDue = a.getDueDate();
                String reformattedDateGoal = a.getGoalDate();
                reformattedDateDue = reformattedDateDue.substring(5) + "-" + reformattedDateDue.substring(0, 4);
                reformattedDateGoal = reformattedDateGoal.substring(5) + "-" + reformattedDateGoal.substring(0, 4);
                dueDate.setText(reformattedDateDue);
                goalDate.setText(reformattedDateGoal);

                if (a.getGoalDateAlert() == 1) {
                    assessAlert.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
                } else {
                    assessAlert.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
                for (Course c : Course.getCourseList()) {
                    if(a.getCourseIdFK() == c.getCourseId()) {
                        courseNum.setText(c.getCourseNum());
                        courseName.setText(c.getName());
                    }
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.assessment_view_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                    intent = new Intent(this, CourseViewActivity.class);
                    startActivity(intent);
                    finish();
                return true;
            case R.id.asses_action_edit:
                fromAssessmentView = true;//todo
                AssessmentListActivity.addAssessmentToList = false;
                intent = new Intent(this, AssessmentEditActivity.class);
                startActivity(intent);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
