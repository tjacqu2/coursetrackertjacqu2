package com.example.tomjacques.coursetracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tomjacques on 1/13/18.
 */

public class CourseListActivity extends AppCompatActivity implements CLAdapterInterface{

    PopupMenu popupMenu;
    PopupMenu.OnMenuItemClickListener menuItemClickListener1;
    DatabaseMgr db;

    private CourseListAdapter myCourseListAdapter;
    public static boolean addNewCourseFromCLAct = false;
    public static boolean editCourse = false;
    public ArrayList<HashMap<String, String>> listCourse;
    public static final String FIRST_COL = "first";
    public static final String SECOND_COL = "second";
    public static final String THIRD_COL = "third";
    public static final String ID_KEY = "fourth";
    public static final String ALERT_START = "alertStart";
    public static final String ALERT_END = "alertEnd";

    private static List<String> termDropDown = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        db = new DatabaseMgr(this);
        Term.getTermList().clear();
        termDropDown.clear();
        Term.setTermList(db.getTermListFromDB());
        for (Term t : Term.getTermList()) {
            if (t.getTermId() != 1) {//skips term 0
                termDropDown.add(Integer.toString(t.getTermNumber()));
                for (Course c : Course.getCourseList()) {
                    if (t.getTermId() == c.getTermIdFK()) {
                        t.getAssociatedCourses().add(c);
                    }
                }
            }
        }

        ListView listView = findViewById(android.R.id.list);
        listCourse = new ArrayList<>();
        listCourse.clear();

        //put data in a hash map to display in columns on screen
        for(Course c : Course.getCourseList()){
            String term = " None";
            for (Term t : Term.getTermList()) {
                if (c.getTermIdFK() == t.getTermId()) {
                    term = Integer.toString(t.getTermNumber());
                }

            }
            //System.out.println(c.getName());
            HashMap<String, String> temp = new HashMap<>();
            temp.put(FIRST_COL, c.getName() + "\n" + c.getCourseNum());
            temp.put(SECOND_COL, term);
            temp.put(THIRD_COL, c.getStatus());
            temp.put(ID_KEY, Integer.toString(c.getCourseId()));
            temp.put(ALERT_START, Integer.toString(c.getAlertStart()));
            temp.put(ALERT_END, Integer.toString(c.getAlertEnd()));
            listCourse.add(temp);
        }

        //display list
        //CourseListAdapter adapter = new CourseListAdapter(this, listCourse, );
        myCourseListAdapter = new CourseListAdapter(this, listCourse, this);
        listView.setAdapter(myCourseListAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.course_list_actionbar, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml

        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
        case R.id.action_add_new:
            addNewCourseFromCLAct = true;
            TermListActivity.termListPos = -1;//-1 set term list indicator to initial position, no term selected
            startActivity(new Intent(this,
                    CourseEditActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public boolean isCourseAssociatedWithTerm() {
        boolean courseHasTerm = true;
        for (Course c : Course.getCourseList()) {
            if (c.getCourseId() == MainListAdapter.courseIdKeyMatch) {
                if (c.getTermIdFK() == -1) {
                    courseHasTerm = false;
                }
            }
        }
        return courseHasTerm;
    }

    public boolean isTermChosenForCourseAdd() {
       boolean addHasTerm = false;
       if (TermListActivity.termListPos > 0) {
           addHasTerm = true;
       }
       return addHasTerm;

    }

    public void addCourseToTerm() {//pass the termListPos in
        boolean add = false;
        int termId = -1;
        db = new DatabaseMgr(this);
        int count = 0;
        for (Term t : Term.getTermList()) {
            if (t.getTermId() > 1) {
                if (TermListActivity.termListPos == count) {//specifies the clicked term
                    for (Course c : Course.getCourseList()) {
                        if (c.getCourseId() == MainListAdapter.courseIdKeyMatch) {//specifies clicked course
                            //System.out.println(c.getTermIdFK());
                            add = true;
                            if (c.getStatus().equals("Dropped")) {
                                c.setStatus("Plan to Take");
                            }
                            c.setTermIdFK(t.getTermId());
                            db.updateCourse(c.getCourseId(),
                                    c.getName(),
                                    c.getCourseNum(),
                                    c.getStatus(),
                                    c.getDateStart(),
                                    c.getDateEnd(),
                                    c.getAlertStart(),
                                    c.getAlertEnd(),
                                    c.getNotes(),
                                    t.getTermId(),
                                    c.getMentorIdFK());
                            termId = t.getTermId();
                        }
                    }
                }
            }
            count++;
        }
        if (add) {
            //todo listLoad method
            Course.getCourseList().clear();
            Course.setCourseList(db.getCourseListFromDB());
            //reload assessments into courses
            for (Course c : Course.getCourseList()) {
                c.getAssociatedAssessments().clear();
                  for (Assessment a : Assessment.getAssessmentList())  {
                      if (c.getCourseId() == a.getCourseIdFK()) {
                          c.getAssociatedAssessments().add(a);
                      }
                  }
            }
            //set associated courses into terms
            for (Term t : Term.getTermList()) {
                if (t.getTermId() == termId) {
                    for (Course c : Course.getCourseList()) {
                        if (c.getCourseId() == MainListAdapter.courseIdKeyMatch) {
                            t.getAssociatedCourses().add(c);
                        }
                    }
                }
            }
            Intent intent = new Intent (this, TermListActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void deleteCourse() {
        AlertDialog.Builder ad;
        db = new DatabaseMgr(this);
        ad = new AlertDialog.Builder(this);
        ad.setTitle("Confirm Delete")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int yes) {
                        //delete from database
                        db.deleteCourseFromDb(MainListAdapter.courseIdKeyMatch);
                        //delete assessments from course being deleted
                        for (Course c : Course.getCourseList()) {
                            if (c.getCourseId() == MainListAdapter.courseIdKeyMatch) {
                                for (Assessment a : Assessment.getAssessmentList()) {
                                    if (a.getCourseIdFK() == c.getCourseId()) {
                                        db.deleteAssessmentFromDb(a.getAssessId());
                                    }
                                }
                            }
                        }
                        Assessment.getAssessmentList().clear();
                        Assessment.setAssessmentList(db.getAssessmentListFromDB());
                        //clear course list
                        Course.getCourseList().clear();
                        //reload course list from db
                        Course.setCourseList(db.getCourseListFromDB());
                        //reload assessment lists into courses
                        //todo listLoad method
                        for (Course c : Course.getCourseList()) {//block to reload course assosciated assessments
                            c.getAssociatedAssessments().clear();
                            for (Assessment a : Assessment.getAssessmentList()) {
                                if (c.getCourseId() == a.getCourseIdFK()) {
                                    c.getAssociatedAssessments().add(a);
                                }
                            }
                        }
                        Intent intent = new Intent (CourseListActivity.this, CourseListActivity.class);
                        startActivity(intent);
                        Toast.makeText(
                                getApplicationContext(),
                                "Course deleted", Toast.LENGTH_SHORT
                        ).show();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int no) {
                        dialog.dismiss();
                    }

                })
                .setCancelable(false)
                .create()
                .show();
    }

    public void courseAssociatedWithTermAlert() {
        AlertDialog.Builder ad;
        ad = new AlertDialog.Builder(this);
        ad.setTitle("Course Already In A Term")
                .setMessage("Course can only be used once")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(
                            DialogInterface dialog,
                            int ok) {
                        //do nothing
                        dialog.dismiss();
                    }

                })
                .setCancelable(false)
                .create()
                .show();
    }

    public void termChoiceAlert() {
        if (Term.getTermList().size() <2) {
            AlertDialog.Builder ad2;
            ad2 = new AlertDialog.Builder(this);
            ad2.setTitle("No Terms Available")
                    .setMessage("Please add a new term")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int ok) {
                            dialog.dismiss();
                        }
                    })
            .create()
            .show();
        }else {
            LayoutInflater inflater = LayoutInflater.from(this);
            View layout_spinner = inflater.inflate(R.layout.term_spinner, null);
            AlertDialog.Builder ad;
            ad = new AlertDialog.Builder(this);
            ad.setView(layout_spinner);
            ad.setTitle("Choose Term");
            ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(
                        DialogInterface dialog,
                        int ok) {
                    addCourseToTerm();
                    dialog.dismiss();
                }

            });
            ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(
                        DialogInterface dialog, int cancel) {
                    TermListActivity.termListPos = -1;//ok
                    dialog.dismiss();
                }
            });
            ad.setCancelable(false);
            ad.create();
            Spinner spinner = layout_spinner.findViewById(R.id.termSpinner);
            ArrayAdapter<String> sAdapter = new ArrayAdapter<String>(CourseListActivity.this,
                    android.R.layout.simple_spinner_item, termDropDown);
            sAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(sAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    TermListActivity.termListPos = position + 1;
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            ad.show();
        }
    }

    @Override
    public void chooseTerm() {
        if (isCourseAssociatedWithTerm()) {
            courseAssociatedWithTermAlert();
        } else {
            if (!isTermChosenForCourseAdd()) {
                termChoiceAlert();
            }
            else {
                addCourseToTerm();
            }
        }
    }
}
