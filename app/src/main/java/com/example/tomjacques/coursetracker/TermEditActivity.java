package com.example.tomjacques.coursetracker;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class TermEditActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
DatePickerDialog.OnDateSetListener{
    DatabaseMgr db;
    //FragmentManager termDatePicker;
    private EditText termName;
    private TextView termNum;
    private String termStart;
    private String termEnd;
    private int termId;
    private int statusSpinnerPos;
    private int termSpinnerPos;
    private int dayStartSpinnerPos;
    private int monthStartSpinnerPos;
    private int yearStartSpinnerPos;
    private int dayEndSpinnerPos;
    private int monthEndSpinnerPos;
    private int yearEndSpinnerPos;
    private TextView termDateStart;
    private TextView termDateEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_edit);

        termNum = findViewById(R.id.termNum);
        termName = findViewById(R.id.termName);
        termDateStart = findViewById(R.id.term_date_start);
        termDateEnd = findViewById(R.id.term_date_end);

        if (!TermListActivity.addNewTermFromTLAct && !MainActivity.addNewTermFromMainAct) {//edit existing term
            for (Term t : Term.getTermList()) {
                if (t.getTermId() == ExpandableTermDataRetrieve.listTermId.get(TermListActivity.termListPos)) {
                    termId = t.getTermId();
                    termName.setText(t.getTermName());
                    termStart = t.getTermStart();
                    termEnd = t.getTermEnd();
                    termNum.setText(Integer.toString(t.getTermNumber()));

                    String tStart = termStart.substring(5, 7) + "-" + termStart.substring(8, 10)
                    + "-" + termStart.substring(0, 4);
                    String tEnd = termEnd.substring(5, 7) + "-" + termEnd.substring(8, 10)
                            + "-" + termEnd.substring(0, 4);

                    //populate text view
                    termDateStart.setText(tStart);
                    termDateEnd.setText(tEnd);
                }
            }
        }
        if (TermListActivity.addNewTermFromTLAct || MainActivity.addNewTermFromMainAct) {
            termNum.setText(Integer.toString(Term.getTermList().size()));
            String dFormat = "mm-dd-yyyy";
            termDateStart.setText(dFormat);
            termDateEnd.setText(dFormat);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.term_edit_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_save_term:
                saveTerm();
                Toast.makeText(
                        getApplicationContext(),
                        "Term saved", Toast.LENGTH_SHORT
                ).show();
                if (TermViewActivity.fromTermView) {
                    TermViewActivity.fromTermView = false;
                    intent = new Intent(this, TermViewActivity.class);
                    startActivity(intent);
                } else if (TermListActivity.addNewTermFromTLAct || MainActivity.addNewTermFromMainAct
                        || TermListActivity.fromTermList){
                    TermListActivity.addNewTermFromTLAct = false;
                    MainActivity.addNewTermFromMainAct = false;
                    TermListActivity.fromTermList = false;
                    intent = new Intent(this, TermListActivity.class);
                    startActivity(intent);
                }
                finish();
                return true;
            case R.id.action_cancel_term:
                if (TermListActivity.addNewTermFromTLAct || TermListActivity.fromTermList) {
                    intent = new Intent(TermEditActivity.this, TermListActivity.class);
                    TermListActivity.addNewTermFromTLAct = false;
                    TermListActivity.fromTermList = false;
                    startActivity(intent);
                    return true;
                }
                else if (TermViewActivity.fromTermView) {
                    intent = new Intent(TermEditActivity.this, TermViewActivity.class);
                    TermViewActivity.fromTermView = false;
                    startActivity(intent);
                    return true;
                }
                else if (MainActivity.addNewTermFromMainAct) {
                    intent = new Intent(TermEditActivity.this, MainActivity.class);
                    MainActivity.addNewTermFromMainAct = false;
                    startActivity(intent);
                }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    //possible fragment setup to change action bar title
        /*private void displayFragment(int position) {
            // update the main_actionbar content by replacing fragments
            Fragment fragment = null;
            String title = "";
            switch (position) {
                case 0:
                    fragment = new Add();
                    title = "Term Add";
                    break;
                case 1:
                    fragment = new Edit();
                    title = "Term Edit";
                    break;
                default:
                    break;
            }

            // update selected fragment and title
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
                getSupportActionBar().setTitle(title);
                // change icon to arrow drawable
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow);
            }
        }*/

    public void showTermStartDatePickerDialog(View v) {
        DatePickerFragment.datePicker = 1;
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.init(this,this);
        newFragment.show(getFragmentManager(), "termStartDatePicker");
    }

    public void showTermEndDatePickerDialog(View v) {
        DatePickerFragment.datePicker = 2;
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.init(this,this);
        newFragment.show(getFragmentManager(), "termEndDatePicker");
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        month += 1;
        String dayString;
        String monthString;
        dayString = String.format("%02d", day);
        monthString = String.format("%02d", month);
        String datePickerString = monthString + "-" + dayString
                + "-" + Integer.toString(year);
        switch(DatePickerFragment.datePicker) {
            case 1:
                termDateStart.setText(datePickerString);
                break;
            case 2:
                termDateEnd.setText(datePickerString);
                break;
        }
    }

    private void saveTerm() {
        String newTermStart = termDateStart.getText().toString().substring(6)
                + "-" + termDateStart.getText().toString().substring(0, 3)
                + termDateStart.getText().toString().substring(3, 5);
        String newTermEnd = termDateEnd.getText().toString().substring(6)
                + "-" + termDateEnd.getText().toString().substring(0, 3)
                + termDateEnd.getText().toString().substring(3, 5);
        //conflicting term alert here?
        db = new DatabaseMgr(this);
        Term term = new Term();
        term.setTermNumber(Term.getTermList().size());
        term.setTermName(termName.getText().toString());
        term.setTermStart(newTermStart);
        term.setTermEnd(newTermEnd);
        if (TermListActivity.addNewTermFromTLAct || MainActivity.addNewTermFromMainAct) {//add new term to db
            db.insertTerm(Term.getTermList().size(),
                    termName.getText().toString(),
                    newTermStart,
                    newTermEnd);
            Term.getTermList().clear();
            Term.setTermList(db.getTermListFromDB());
            TermListActivity.termListPos = Term.getTermList().size() - 1;// -1 ok todo
        } else if (!TermListActivity.addNewTermFromTLAct && !MainActivity.addNewTermFromMainAct){//update existing term in db
            db.updateTerm (termId, Integer.parseInt(termNum.getText().toString()),
                    termName.getText().toString(),
                    newTermStart,
                    newTermEnd);
            Term.getTermList().clear();
            Term.setTermList(db.getTermListFromDB());
        }
        //todo listLoad method
        for (Term t : Term.getTermList()) {
            t.getAssociatedCourses().clear();
            for (Course c : Course.getCourseList()) {
                if (c.getTermIdFK() == t.getTermId()) {
                    t.getAssociatedCourses().add(c);
                }
            }
        }

    }

    public void setTermDateStart(TextView termDateStart) {
        this.termDateStart = termDateStart;
    }
}
