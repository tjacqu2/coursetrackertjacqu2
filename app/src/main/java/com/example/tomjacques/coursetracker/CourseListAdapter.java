package com.example.tomjacques.coursetracker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.PopupMenu;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.tomjacques.coursetracker.CourseListActivity.ALERT_END;
import static com.example.tomjacques.coursetracker.CourseListActivity.ALERT_START;
import static com.example.tomjacques.coursetracker.CourseListActivity.FIRST_COL;
import static com.example.tomjacques.coursetracker.CourseListActivity.ID_KEY;
import static com.example.tomjacques.coursetracker.CourseListActivity.SECOND_COL;
import static com.example.tomjacques.coursetracker.CourseListActivity.THIRD_COL;
/**
 * Created by tomjacques on 1/28/18.
 */

public class CourseListAdapter extends BaseAdapter {
    DatabaseMgr db;
    public static boolean deleteCourse = false;
    public static boolean addCourseToTerm = false;
    public ArrayList<HashMap<String, String>> list;
    private CLAdapterInterface listener;
    Activity activity;
    TextView txtFirst;
    TextView txtSecond;
    TextView txtThird;
    String courseIdKey;
    String alertStart;
    String alertEnd;

    public CourseListAdapter(Activity activity, ArrayList<HashMap<String, String>> list, CLAdapterInterface listener) {
        super();
        this.list = list;
        this.activity = activity;
        this.listener = listener;
    }

    @Override
    public int getViewTypeCount() {
        if (getCount() < 1) {
            return 1;
        }
        return getCount();//was getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView( final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.content_course_list, null);

            txtFirst = convertView.findViewById(R.id.course_column);
            txtSecond = convertView.findViewById(R.id.term_column2);
            txtThird = convertView.findViewById(R.id.status_column);
        }

        HashMap<String, String> map = list.get(position);
        alertStart = map.get(ALERT_START);
        alertEnd = map.get(ALERT_END);
        if (Integer.parseInt(alertStart) == 1 || Integer.parseInt(alertEnd) == 1) {
            txtFirst.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
        }
        txtFirst.setText(map.get(FIRST_COL));
        txtSecond.setText(map.get(SECOND_COL));
        txtThird.setText(map.get(THIRD_COL));
        if (txtThird.getText().equals("Plan to Take")) {
            txtThird.setTextColor(Color.BLACK);
        }
        else if (txtThird.getText().equals("In Progress")) {
            txtThird.setTextColor(Color.GREEN);
        }
        else if (txtThird.getText().equals("Completed")) {
            txtThird.setTextColor(Color.BLUE);
        }
        else if (txtThird.getText().equals("Dropped")) {
            txtThird.setTextColor(Color.RED);
        }
        courseIdKey = map.get(ID_KEY);

        try {
            txtFirst.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    switch (v.getId()) {
                        case R.id.course_column:

                            MainListAdapter.courseIdKeyMatch = Integer.parseInt(list.get(position).get(ID_KEY));
                            PopupMenu popup = new PopupMenu(activity, v);
                            popup.getMenuInflater().inflate(R.menu.popup_menu_courselist_course,
                                    popup.getMenu());
                            popup.show();
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {

                                    switch (item.getItemId()) {
                                        case R.id.action_course_details_fromlist:
                                            Intent intent = new Intent(activity, CourseViewActivity.class);
                                            activity.startActivity(intent);
                                            return true;
                                        case R.id.action_assessment_fromlist:
                                            intent = new Intent(activity, AssessmentListActivity.class);
                                            activity.startActivity(intent);
                                            return true;
                                        case R.id.action_notes_fromlist:
                                            intent = new Intent(activity, NotesViewActivity.class);
                                            activity.startActivity(intent);
                                            return true;
                                        case R.id.action_delete_course_fromlist:
                                            listener.deleteCourse();
                                            return true;
                                        case R.id.action_add_course_fromlist:
                                            listener.chooseTerm();
                                            return true;
                                        case R.id.action_edit_course:
                                            CourseListActivity.editCourse = true;
                                            intent = new Intent(activity, CourseEditActivity.class);
                                            activity.startActivity(intent);
                                            return true;
                                        default:
                                            return false;
                                    }
                                }
                            });

                            break;

                        default:
                            break;
                    }


                }
            });

        } catch (Exception e) {

            e.printStackTrace();
        }

        return convertView;
    }
}
