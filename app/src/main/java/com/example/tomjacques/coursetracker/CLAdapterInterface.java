package com.example.tomjacques.coursetracker;

/**
 * Created by tomjacques on 3/13/18.
 * Needed to be implemented in order to access CourseListActivity
 * methods from the CourseListAdapter
 */

public interface CLAdapterInterface {
    void chooseTerm();
    void deleteCourse();
}
