package com.example.tomjacques.coursetracker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import static com.example.tomjacques.coursetracker.MainActivity.ALERT_END;
import static com.example.tomjacques.coursetracker.MainActivity.ALERT_START;
import static com.example.tomjacques.coursetracker.MainActivity.FIRST_COL;
import static com.example.tomjacques.coursetracker.MainActivity.SECOND_COL;
import static com.example.tomjacques.coursetracker.MainActivity.ID_KEY;
import static com.example.tomjacques.coursetracker.MainActivity.STATUS;

/**
 * Created by tomjacques on 2/2/18.
 */

public class MainListAdapter extends BaseAdapter{
    MLAdapterInterface listener;
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    TextView txtFirst;
    TextView txtSecond;
    String courseIdKey;
    String alertStart;
    String alertEnd;
    String status;
    public static Boolean dropCourse = false;
    public static int courseIdKeyMatch;

    public MainListAdapter(Activity activity, ArrayList<HashMap<String, String>> list, MLAdapterInterface listener) {
        super();
        this.activity = activity;
        this.list = list;
        this.listener = listener;
    }

    @Override
    public int getViewTypeCount() {
        if (getCount() < 1) {
            return 1;
        }
        return getCount();//was getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = activity.getLayoutInflater();

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.content_main, null);

            txtFirst = convertView.findViewById(R.id.main_column1);
            txtSecond = convertView.findViewById(R.id.main_column2);
        }

        HashMap<String, String> map = list.get(position);
        alertStart = map.get(ALERT_START);
        alertEnd = map.get(ALERT_END);
        status = map.get(STATUS);
        if (Integer.parseInt(alertStart) == 1 || Integer.parseInt(alertEnd) == 1) {
            txtFirst.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
        }
        txtFirst.setText(map.get(FIRST_COL));
        txtSecond.setText(map.get(SECOND_COL));
        if (status.equals("Plan to Take")) {
            txtSecond.setTextColor(Color.BLACK);
        }
        else if (status.equals("In Progress")) {
            txtSecond.setTextColor(Color.GREEN);
        }
        else if (status.equals("Completed")) {
            txtSecond.setTextColor(Color.BLUE);
        }
        else if (status.equals("Dropped")) {
            txtSecond.setTextColor(Color.RED);
        }
        courseIdKey = map.get(ID_KEY);

        try {
            txtFirst.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    switch (v.getId()) {
                        case R.id.main_column1:
                            courseIdKeyMatch = Integer.parseInt(list.get(position).get(ID_KEY));
                                PopupMenu popup = new PopupMenu(activity, v);
                                popup.getMenuInflater().inflate(R.menu.popup_menu_main,
                                        popup.getMenu());
                                popup.show();
                                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                    @Override
                                    public boolean onMenuItemClick(MenuItem item) {

                                        switch (item.getItemId()) {
                                            case R.id.action_course_details:
                                                Intent intent = new Intent(activity, CourseViewActivity.class);
                                                activity.startActivity(intent);
                                                return true;
                                            case R.id.action_view_add_notes:
                                                intent = new Intent(activity, NotesViewActivity.class);
                                                activity.startActivity(intent);
                                                return true;
                                            case R.id.action_add_assess:
                                                intent = new Intent(activity, AssessmentListActivity.class);
                                                activity.startActivity(intent);
                                                activity.finish();
                                                return true;
                                            case R.id.action_edit_course:
                                                MainActivity.editCourse = true;
                                                intent = new Intent(activity, CourseEditActivity.class);
                                                activity.startActivity(intent);
                                                return true;
                                            case R.id.action_drop_course:
                                                listener.dropCourse(MainListAdapter.courseIdKeyMatch);
                                                intent = new Intent(activity, MainActivity.class);
                                                activity.startActivity(intent);
                                                return true;
                                            default:
                                                return false;
                                        }
                                    }
                                });
                            //}
                            break;

                        default:
                            break;
                    }


                }
            });

        } catch (Exception e) {

            e.printStackTrace();
        }

        return convertView;
    }
}
