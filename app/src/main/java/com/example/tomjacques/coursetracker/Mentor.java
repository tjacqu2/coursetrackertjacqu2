package com.example.tomjacques.coursetracker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomjacques on 1/21/18.
 */

public class Mentor {

    private int id;
    private String mentorName;
    private String mentorEmail;
    private String mentorPhone;
    private static List<Mentor> mentorList = new ArrayList<>();

    public Mentor() {

    }

    public Mentor(int id, String mentorName, String mentorEmail, String mentorPhone) {
        this.id = id;
        this.mentorName = mentorName;
        this.mentorEmail = mentorEmail;
        this.mentorPhone = mentorPhone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMentorName() {
        return mentorName;
    }

    public void setMentorName(String mentorName) {
        this.mentorName = mentorName;
    }

    public String getMentorEmail() {
        return mentorEmail;
    }

    public void setMentorEmail(String mentorEmail) {
        this.mentorEmail = mentorEmail;
    }

    public String getMentorPhone() {
        return mentorPhone;
    }

    public void setMentorPhone(String mentorPhone) {
        this.mentorPhone = mentorPhone;
    }

    public static List<Mentor> getMentorList() {
        return mentorList;
    }

    public static void setMentorList(List<Mentor> mentorList) {
        Mentor.mentorList = mentorList;
    }
}
