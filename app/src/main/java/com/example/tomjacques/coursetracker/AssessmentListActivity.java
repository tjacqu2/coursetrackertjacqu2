package com.example.tomjacques.coursetracker;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

//import static com.example.tomjacques.coursetracker.AssessmentListAdapter.txtFirst;

/**
 * Created by tomjacques on 1/13/18.
 */

public class AssessmentListActivity extends AppCompatActivity implements ALAdapterInterface{//implements OnTouchListener
    private TextView assessCourseName;
    private TextView assessCourseNum;
    private AssessmentListAdapter myAssessmentListAdapter;
    public static boolean addAssessmentToList = false;
    DatabaseMgr db;
    public ArrayList<HashMap<String, String>> list;
    public static final String FIRST_COL = "first";
    public static final String SECOND_COL = "second";
    public static final String THIRD_COL = "third";
    public static final String ID_KEY = "id";
    public static final String AID_KEY = "aid";
    public static final String ALERT_KEY = "alert";
    private long mLastDownEventTime = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        addAssessmentToList = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (AssessmentListAdapter.deleteAssess) {
            deleteAssessment();
            AssessmentListAdapter.deleteAssess = false;
        }

        ListView listView = findViewById(android.R.id.list);
        list = new ArrayList<>();
        list.clear();
        db = new DatabaseMgr(this);

        //get data from table
        for (Course c : Course.getCourseList()) {
            if (MainListAdapter.courseIdKeyMatch == c.getCourseId()) {
                assessCourseName = findViewById(R.id.courseNameAssess);
                assessCourseNum = findViewById(R.id.courseNumAssess);
                assessCourseName.setText(c.getName());
                assessCourseNum.setText(c.getCourseNum());
                for (Assessment a : c.getAssociatedAssessments()) {
                    HashMap<String, String> temp = new HashMap<>();
                    temp.put(FIRST_COL, a.getAssessName());
                    temp.put(SECOND_COL, a.getAssessType());
                    temp.put(THIRD_COL, Integer.toString(a.getQuestions()));
                    temp.put(ID_KEY, Integer.toString(a.getCourseIdFK()));
                    temp.put(AID_KEY, Integer.toString(a.getAssessId()));
                    temp.put(ALERT_KEY, Integer.toString(a.getGoalDateAlert()));
                    list.add(temp);
                }
            }
        }
        myAssessmentListAdapter = new AssessmentListAdapter(this, list, this);
        listView.setAdapter(myAssessmentListAdapter);
    }

    @Override
    public void deleteAssessment() {
        AlertDialog.Builder ad;
        db = new DatabaseMgr(this);
        ad = new AlertDialog.Builder(this);
        ad.setTitle("Confirm Delete")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int yes) {
                        db.deleteAssessmentFromDb(AssessmentListAdapter.assessIdMatch);
                        Assessment.getAssessmentList().clear();
                        Assessment.setAssessmentList(db.getAssessmentListFromDB());
                        for (Course c : Course.getCourseList()) {
                            if (AssessmentListAdapter.courseIdFKMatch == c.getCourseId()) {
                                c.getAssociatedAssessments().clear();
                                for (Assessment a : Assessment.getAssessmentList()) {
                                    if (a.getCourseIdFK() == c.getCourseId()) {
                                        c.getAssociatedAssessments().add(a);
                                    }
                                }
                            }
                        }
                        Intent intent = new Intent(AssessmentListActivity.this, AssessmentListActivity.class);
                        startActivity(intent);
                        Toast.makeText(
                                getApplicationContext(),
                                "Assessment deleted", Toast.LENGTH_SHORT
                        ).show();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int no) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.assessment_list_actionbar, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                    intent = new Intent(this, CourseViewActivity.class);
                    startActivity(intent);
                    finish();
                return true;
            case R.id.action_add_new_assess:
                addAssessmentToList = true;
                intent = new Intent(this, AssessmentEditActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
