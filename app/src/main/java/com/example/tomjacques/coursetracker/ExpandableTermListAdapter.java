package com.example.tomjacques.coursetracker;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by tomjacques on 2/9/18.
 */

public class ExpandableTermListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<String> expandableListHeading;
    private LinkedHashMap<String, List<String>> expandableListCourses;

    public ExpandableTermListAdapter (Context context, List<String> expandableListHeading,
                                      LinkedHashMap<String, List<String>> expandableListCourses) {
        this.context = context;
        this.expandableListHeading = expandableListHeading;
        this.expandableListCourses = expandableListCourses;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListCourses.get(this.expandableListHeading.get(listPosition))
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = (String)getChild(listPosition, expandedListPosition);
        int colorView = R.id.expandedListItem;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (!TermViewActivity.color) {
                convertView = layoutInflater.inflate(R.layout.term_items, null);
                colorView = R.id.expandedListItem;
            } else if (TermViewActivity.color){
                convertView = layoutInflater.inflate(R.layout.term_items2, null);
                colorView = R.id.expandedListItem2;
            }
        }
            TextView expandedListTextView = (TextView) convertView.findViewById(colorView);
            expandedListTextView.setText(expandedListText);
            if (!TermViewActivity.color) {
                for (Course c : Course.getCourseList()) {
                    String match = c.getName() + " " + c.getCourseNum();
                    if (match.equals(expandedListText)) {
                        if (c.getStatus().equals("Completed")) {
                            expandedListTextView.setTextColor(Color.BLUE);
                        } else if (c.getStatus().equals("Plan to Take")) {
                            expandedListTextView.setTextColor(Color.BLACK);
                        } else if (c.getStatus().equals("In Progress")) {
                            expandedListTextView.setTextColor(Color.GREEN);
                        } else if (c.getStatus().equals("Dropped")) {
                            expandedListTextView.setTextColor(Color.RED);
                        }
                    }
                }
            }
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListCourses.get(this.expandableListHeading.get(listPosition))
                .size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListHeading.get(listPosition);
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public int getGroupCount() {
        return this.expandableListHeading.size();
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.term_header, null);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.listTitle);
        listTitleTextView.setText(listTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandeListPosition) {
        return true;
    }
}
