package com.example.tomjacques.coursetracker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomjacques on 1/20/18.
 */

public class Assessment {

    private int assessId;
    private String assessName;
    private String assessType;
    private int questions;
    private String passFailCriteria;
    private String myScore;
    private String dueDate;
    private String goalDate;
    private int goalDateAlert;
    private int courseIdFK;
    private static List<Assessment> assessmentList = new ArrayList<>();

    public Assessment() {

    }

    public Assessment(int assessId, String assessName, String assessType, int questions,
                      String passFailCriteria, String myScore, String dueDate, String goalDate, int goalDateAlert, int courseIdFK) {
        this.assessId = assessId;
        this.assessName = assessName;
        this.assessType = assessType;
        this.questions = questions;
        this.passFailCriteria = passFailCriteria;
        this.myScore = myScore;
        this.dueDate = dueDate;
        this.goalDate = goalDate;
        this.goalDateAlert = goalDateAlert;
        this.courseIdFK = courseIdFK;
    }

    public int getAssessId() {
        return assessId;
    }

    public void setAssessId(int assessId) {
        this.assessId = assessId;
    }

    public String getAssessName() {
        return assessName;
    }

    public void setAssessName(String assessName) {
        this.assessName = assessName;
    }

    public String getAssessType() {
        return assessType;
    }

    public void setAssessType(String assessType) {
        this.assessType = assessType;
    }

    public int getQuestions() {
        return questions;
    }

    public void setQuestions(int questions) {
        this.questions = questions;
    }

    public String getPassFailCriteria() {
        return passFailCriteria;
    }

    public void setPassFailCriteria(String passFailCriteria) {
        this.passFailCriteria = passFailCriteria;
    }

    public String getMyScore() {
        return myScore;
    }

    public void setMyScore(String myScore) {
        this.myScore = myScore;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getGoalDate() {
        return goalDate;
    }

    public void setGoalDate(String goalDate) {
        this.goalDate = goalDate;
    }

    public int getGoalDateAlert() {
        return goalDateAlert;
    }

    public void setGoalDateAlert(int goalDateAlert) {
        this.goalDateAlert = goalDateAlert;
    }

    public int getCourseIdFK() {
        return courseIdFK;
    }

    public void setCourseIdFK(int courseIdFK) {
        this.courseIdFK = courseIdFK;
    }

    public static List<Assessment> getAssessmentList() {
        return assessmentList;
    }

    public static void setAssessmentList(List<Assessment> assessmentList) {
        Assessment.assessmentList = assessmentList;
    }
}
