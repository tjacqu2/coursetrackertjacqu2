package com.example.tomjacques.coursetracker;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import static com.example.tomjacques.coursetracker.AssessmentListActivity.AID_KEY;
import static com.example.tomjacques.coursetracker.AssessmentListActivity.ALERT_KEY;
import static com.example.tomjacques.coursetracker.AssessmentListActivity.FIRST_COL;
import static com.example.tomjacques.coursetracker.AssessmentListActivity.ID_KEY;
import static com.example.tomjacques.coursetracker.AssessmentListActivity.SECOND_COL;
import static com.example.tomjacques.coursetracker.AssessmentListActivity.THIRD_COL;
//import static com.example.tomjacques.coursetracker.AssessmentListActivity.courseIdFK;

/**
 * Created by tomjacques on 1/28/18.
 */

public class AssessmentListAdapter extends BaseAdapter {
    private ALAdapterInterface listener;
    public static boolean deleteAssess = false;
    private ArrayList<HashMap<String, String>> list;
    private Activity activity;
    private TextView txtFirst;
    private TextView txtSecond;
    private TextView txtThird;
    private String courseIdFK;
    private String assessId;
    private String alert;
    public static int courseIdFKMatch;
    public static int assessIdMatch;

    public AssessmentListAdapter(Activity activity, ArrayList<HashMap<String, String>> list, ALAdapterInterface listener) {
        super();
        this.list = list;
        this.activity = activity;
        this.listener = listener;
    }

    @Override
    public int getViewTypeCount() {
        if (getCount() < 1) {
            return 1;
        }
        return getCount();//was getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getCount() { return list.size();}

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.content_assessment_list,null);

            txtFirst = convertView.findViewById(R.id.assess_column);
            txtSecond = convertView.findViewById(R.id.type_column);
            txtThird = convertView.findViewById(R.id.questions_column);
        }

        HashMap<String, String> map = list.get(position);
        alert = map.get(ALERT_KEY);
        txtFirst.setText(map.get(FIRST_COL));
        if (Integer.parseInt(alert) == 1) {
            txtFirst.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
        }
        txtSecond.setText(map.get(SECOND_COL));
        txtThird.setText(map.get(THIRD_COL));
        courseIdFK = map.get(ID_KEY);
        assessId = map.get(AID_KEY);
        try {
            txtFirst.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    courseIdFKMatch = Integer.parseInt(list.get(position).get(ID_KEY));
                    assessIdMatch = Integer.parseInt(list.get(position).get(AID_KEY));
                    switch (v.getId()) {
                        case R.id.assess_column:

                            PopupMenu popup = new PopupMenu(activity, v);
                            popup.getMenuInflater().inflate(R.menu.popup_menu_assess_list,
                                    popup.getMenu());
                            popup.show();
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {

                                    switch (item.getItemId()) {
                                        case R.id.action_assessment_details:
                                            Intent intent = new Intent(activity, AssessmentViewActivity.class);
                                            activity.startActivity(intent);
                                            return true;
                                        case R.id.action_delete_assessment:
                                            listener.deleteAssessment();
                                            return true;
                                        case R.id.action_edit_assessment:
                                            intent = new Intent(activity, AssessmentEditActivity.class);
                                            activity.startActivity(intent);
                                            return true;
                                        default:
                                            return false;
                                    }
                                }
                            });

                            break;

                        default:
                            break;
                    }


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }
}
