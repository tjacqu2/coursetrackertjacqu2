package com.example.tomjacques.coursetracker;


import android.support.v7.app.AppCompatActivity;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by tomjacques on 2/9/18.
 */

public class ExpandableTermDataRetrieve extends AppCompatActivity{
    public static List<Integer> listTermId = new ArrayList<>();
    public static List<Integer> expandableCourseList = new ArrayList<>();
    public static LinkedHashMap<String, List<String>> getTermData() {
        String nameCheck;
        listTermId.clear();
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<String, List<String>>();//Linked puts list in order
        for (Term t : Term.getTermList()) {
            if(t.getTermName().equals("")) {
                nameCheck = "";
            } else {
                nameCheck = t.getTermName() + "\n";
            }
            if (t.getTermNumber() != 0) {
                List<String> courses;
                courses = courseListIterator(t.getTermId(), t.getAssociatedCourses());
                expandableListDetail.put(nameCheck + "Term " + t.getTermNumber() + "   " + dateReformat(t.getTermStart()) + " to " + dateReformat(t.getTermEnd()), courses);
            }
            listTermId.add(t.getTermId());
        }
        return expandableListDetail;
    }

    private static String dateReformat(String date) {//from yyyy-MM-dd to MM-dd-yyyy
        String reformattedDate;
        reformattedDate = date.substring(5) + "-" + date.substring(0, 4);
        return reformattedDate;
    }


    public static LinkedHashMap<String, List<String>> getSingleTermData(int listPosition) {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<String, List<String>>();
        List<String> courses = new ArrayList<>();
        String nameCheck;
        expandableCourseList.clear();
        for (Term t : Term.getTermList()) {
            if(t.getTermName().equals("")) {
                nameCheck = "";
            } else {
                nameCheck = t.getTermName() + "\n";
            }
            if (t.getTermId() == listTermId.get(listPosition)) {
                for (Course c : t.getAssociatedCourses()) {
                        expandableCourseList.add(c.getCourseId());
                        courses.add(c.getName() + " " + c.getCourseNum());//adds to expandable child list view
                }
                expandableListDetail.put(nameCheck + "Term " + t.getTermNumber() + "   " + dateReformat(t.getTermStart()) + " to " + dateReformat(t.getTermEnd()), courses);
            }
        }
        return expandableListDetail;
    }

    private static List<String> courseListIterator(int termId, List<Course> courseList) {
        List<String> courses = new ArrayList<>();
        for (Course c : courseList) {
            if (termId == c.getTermIdFK()) {
                courses.add(c.getName() + " " + c.getCourseNum());
            }
        }
        return courses;
    }
}
//attempt to set color of "Term " to light blue, will revisit
                /*SpannableStringBuilder ssb = new SpannableStringBuilder();
                String termNum = "Term " + Integer.toString(t.getTermNumber()) + " ";
                SpannableString spanStringBlue = new SpannableString(termNum);
                spanStringBlue.setSpan(new ForegroundColorSpan(Color.BLUE), 0, termNum.length(), 0);
                ssb.append(spanStringBlue);
                String dates = dateReformat(t.getTermStart() + " to " + dateReformat(t.getTermEnd()));
                SpannableString spanStringBlack = new SpannableString(dates);
                spanStringBlack.setSpan(new ForegroundColorSpan(Color.BLACK), 0, dates.length(), 0);
                ssb.append(spanStringBlack);*/