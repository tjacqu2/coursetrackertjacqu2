package com.example.tomjacques.coursetracker;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CourseEditActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
        DatePickerDialog.OnDateSetListener{
    DatabaseMgr db;
    private TextView cStartDate;
    private TextView cEndDate;
    private TextView mentorList;
    private TextView dateStartText;
    private TextView dateEndText;
    private RadioButton startSet;
    private RadioButton startClear;
    private RadioButton endSet;
    private RadioButton endClear;
    private EditText courseNum;
    private EditText courseName;
    private String dateStart;
    private String dateEnd;
    private EditText mentorName;
    private EditText mentorEmail;
    private EditText mentorPhone;
    private int courseId;
    private String courseNotes;
    private String mentorTemp = "Select Mentor";
    private int alertStart = 0;
    private int alertEnd = 0;
    private int mentorSpinnerPos;
    private int statusSpinnerPos;
    private int termSpinnerPos;
    private int dayStartSpinnerPos;
    private int monthStartSpinnerPos;
    private int yearStartSpinnerPos;
    private int dayEndSpinnerPos;
    private int monthEndSpinnerPos;
    private int yearEndSpinnerPos;
    private static List<String> mentorsDropDown = new ArrayList<>();
    private static List<String> termDropDown = new ArrayList<>();
    private static String[] statusDropDown = {"Plan to Take", "In Progress", "Completed", "Dropped"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_edit);
        cStartDate = findViewById(R.id.course_start_date);
        cEndDate = findViewById(R.id.course_end_date);
        courseNum = findViewById(R.id.editNumber);
        courseName = findViewById(R.id.editName);
        mentorList = findViewById(R.id.mentor_list);
        mentorName = findViewById(R.id.editMentorName);
        mentorEmail = findViewById(R.id.editMentorEmail);
        mentorPhone = findViewById(R.id.editMentorPhone);
        dateStartText = findViewById(R.id.startDateText);
        dateEndText = findViewById(R.id.endDateText);
        startSet = findViewById(R.id.setStartAlert);
        startClear = findViewById(R.id.clearStartAlert);
        endSet = findViewById(R.id.setEndAlert);
        endClear = findViewById(R.id.clearEndAlert);
        db = new DatabaseMgr(this);

        //get mentor list and populate spinner array
        Mentor.getMentorList().clear();
        mentorsDropDown.clear();
        Mentor.setMentorList(db.getMentorListFromDB());
            for (Mentor m : Mentor.getMentorList()) {
                mentorsDropDown.add(m.getMentorName());
            }

        //get active term numbers and populate spinner array
        Term.getTermList().clear();
        termDropDown.clear();
        Term.setTermList(db.getTermListFromDB());
            for (Term t : Term.getTermList()) {
                if (t.getTermId() == 1) {termDropDown.add("None");}//was t.getTermNumber == 0
                else {termDropDown.add(Integer.toString(t.getTermNumber()));}
                for (Course c : Course.getCourseList()) {
                    if (t.getTermId() == c.getTermIdFK()) {
                        t.getAssociatedCourses().add(c);
                    }
                }
            }
        //mentor spinner
        final Spinner spinnerMentor = (Spinner)findViewById(R.id.mentor_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(CourseEditActivity.this,
                R.layout.spinner_item2, mentorsDropDown);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMentor.setAdapter(adapter);
        spinnerMentor.setOnItemSelectedListener(this);
        //status spinner
        Spinner spinnerStatus = (Spinner)findViewById(R.id.status_spinner);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(CourseEditActivity.this,
                R.layout.spinner_item2, statusDropDown);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(adapter2);
        spinnerStatus.setOnItemSelectedListener(this);
        //term spinner
        Spinner spinnerTerm = (Spinner)findViewById(R.id.term_spinner);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(CourseEditActivity.this,
                R.layout.spinner_item2, termDropDown);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTerm.setAdapter(adapter3);
        spinnerTerm.setOnItemSelectedListener(this);

        if (!MainActivity.addNewCourse && !CourseListActivity.addNewCourseFromCLAct) {//populate existing course fields
            for (Course c : Course.getCourseList()) {
                if (MainListAdapter.courseIdKeyMatch == c.getCourseId()) {
                    courseNum.setText(c.getCourseNum());
                    courseName.setText(c.getName());
                    courseId = c.getCourseId();
                    courseNotes = c.getNotes();
                    //send course status to drop down
                    spinnerStatus.setSelection(Arrays.asList(statusDropDown).indexOf(c.getStatus()));
                    //populate date spinners
                    dateStart = c.getDateStart();
                    dateEnd = c.getDateEnd();

                    String cStart = dateStart.substring(5, 7) + "-" + dateStart.substring(8, 10) + "-" + dateStart.substring(0, 4);
                    String cEnd = dateEnd.substring(5, 7) + "-" + dateEnd.substring(8, 10) + "-" + dateEnd.substring(0, 4);

                    cStartDate.setText(cStart);
                    cEndDate.setText(cEnd);

                    //set radio buttons
                    if(c.getAlertStart() == 1) {
                        dateStartText.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
                        startSet.setChecked(true);
                    } else {
                        dateStartText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        startClear.setChecked(true);
                    }
                    if(c.getAlertEnd() == 1) {
                        dateEndText.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
                        endSet.setChecked(true);
                    } else {
                        dateEndText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        endClear.setChecked(true);
                    }
                    //set mentor fields
                    int count = 0;//count to set mentor spinner selection
                    for (Mentor m : Mentor.getMentorList()) {
                        if (m.getId() == c.getMentorIdFK()) {
                            spinnerMentor.setSelection(count);
                            mentorName.setText(m.getMentorName());
                            mentorEmail.setText(m.getMentorEmail());
                            mentorPhone.setText(m.getMentorPhone());
                            mentorTemp = mentorName.getText().toString();
                        }
                        ++count;
                    }
                    //set term spinner
                    int count2 = 0;//count to set term spinner selection
                    for (Term t : Term.getTermList()) {
                        if (t.getTermId() == c.getTermIdFK()) {
                            spinnerTerm.setSelection(count2);
                        }
                        ++count2;
                    }
                }
            }
        }
        if (MainActivity.addNewCourse || CourseListActivity.addNewCourseFromCLAct) {
            String dFormat = "mm-dd-yyyy";
            cStartDate.setText(dFormat);
            cEndDate.setText(dFormat);
        }
        mentorList.setOnClickListener(new View.OnClickListener() {
            boolean mentorClear = false;
            @Override
            public void onClick(View v) {
            PopupMenu popup = new PopupMenu(CourseEditActivity.this, v);
            popup.getMenuInflater().inflate(R.menu.delete_mentor_popup, popup.getMenu());
            popup.show();
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_delete_mentor:
                                AlertDialog.Builder ad;

                                ad = new AlertDialog.Builder(CourseEditActivity.this);

                                ad.setTitle("Confirm Delete")
                                        .setMessage("Are you sure?")
                                        .setPositiveButton("Yes",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(
                                                            DialogInterface dialog,
                                                            int yes) {
                                                        if (mentorSpinnerPos == 0) {
                                                            AlertDialog.Builder ad2;
                                                            ad2 = new AlertDialog.Builder(CourseEditActivity.this);
                                                            ad2.setTitle("Make Selection")
                                                                    .setMessage("A mentor must be selected")
                                                                    .setPositiveButton("Ok",
                                                                            new DialogInterface.OnClickListener() {
                                                                                public void onClick(
                                                                                        DialogInterface dialog,
                                                                                        int yes) {
                                                                                    //do nothing
                                                                                    dialog.dismiss();
                                                                                }
                                                                            })
                                                                    .create()
                                                                    .show();
                                                        }
                                                        else {
                                                            //delete mentor
                                                            for (Mentor m : Mentor.getMentorList()) {
                                                                if (m.getMentorName().equals(mentorsDropDown.get(mentorSpinnerPos))) {
                                                                    db.deleteMentorFromTable(m.getId());
                                                                    for (Course c : Course.getCourseList()) {
                                                                        if (c.getMentorIdFK() == m.getId()) {
                                                                           c.setMentorIdFK(-1);
                                                                        }
                                                                    }
                                                                    mentorClear = true;
                                                                }
                                                            }
                                                            if (mentorClear) {
                                                                Mentor.getMentorList().clear();
                                                                mentorsDropDown.clear();
                                                                Mentor.setMentorList(db.getMentorListFromDB());
                                                                for (Mentor m : Mentor.getMentorList()) {
                                                                    mentorsDropDown.add(m.getMentorName());
                                                                }
                                                                spinnerMentor.setSelection(0);
                                                                mentorName.setText("");
                                                                mentorEmail.setText("");
                                                                mentorPhone.setText("");
                                                                mentorTemp = mentorName.getText().toString();
                                                                mentorClear = false;
                                                            }
                                                            System.out.println("delete");
                                                            Toast.makeText(
                                                                    getApplicationContext(),
                                                                    "Mentor deleted", Toast.LENGTH_SHORT
                                                            ).show();
                                                            dialog.dismiss();
                                                        }
                                                    }
                                                })
                                        .setNegativeButton("No",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(
                                                            DialogInterface dialog,
                                                            int no) {
                                                        //do nothing
                                                        dialog.dismiss();
                                                    }
                                                })
                                        .setCancelable(false)
                                        .create()
                                        .show();
                                return true;
                            default:
                                return false;
                        }
                    }
                });
            }
        });
    }

    public void radioStartClicked(View v) {
        boolean checked = ((RadioButton)v).isChecked();
        dateStartText = findViewById(R.id.startDateText);
        switch(v.getId()) {
            case R.id.setStartAlert:
                if(checked) {
                    dateStartText.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
                    alertStart = 1;
                }
                break;

            case R.id.clearStartAlert:
                if(checked) {
                    dateStartText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    alertStart = 0;
                }
                break;
        }
    }

    public void radioEndClicked(View v) {
        boolean checked = ((RadioButton)v).isChecked();
        dateEndText = findViewById(R.id.endDateText);
        switch(v.getId()) {
            case R.id.setEndAlert:
                if(checked) {
                    dateEndText.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
                    alertEnd = 1;
                }
                break;

            case R.id.clearEndAlert:
                if(checked) {
                    dateEndText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    alertEnd = 0;
                }
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        switch (parent.getId()) {
            case R.id.mentor_spinner:
            for (Mentor m : Mentor.getMentorList()) {
                if (mentorsDropDown.get(position).equals("Select Mentor")) {
                    mentorName.setText("");
                    mentorEmail.setText("");
                    mentorPhone.setText("");
                }// do nothing
                else if (mentorsDropDown.get(position).equals(m.getMentorName())) {
                    mentorName.setText(m.getMentorName());
                    mentorEmail.setText(m.getMentorEmail());
                    mentorPhone.setText(m.getMentorPhone());
                    mentorTemp = mentorsDropDown.get(position);
                }
                mentorSpinnerPos = position;
            }
            break;
            case R.id.status_spinner:
            statusSpinnerPos = position;
            if (position == 3) {
                termSpinnerPos = 0;
            }
            break;
            case R.id.term_spinner:
            termSpinnerPos = position;
            if (position != 0 && statusSpinnerPos == 3) {
                statusSpinnerPos = 0;
            }
            break;
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.course_edit_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_save_course:
                saveCourse();
                return true;
            case R.id.action_cancel_course:
                //clear fields
                clearFields();
                if (CourseListActivity.addNewCourseFromCLAct || CourseListActivity.editCourse) {
                    intent = new Intent(this, CourseListActivity.class);
                    CourseListActivity.editCourse = false;
                    CourseListActivity.addNewCourseFromCLAct = false;
                    startActivity(intent);
                    return true;
                }
                else if (CourseViewActivity.fromCourseView) {
                    intent = new Intent(CourseEditActivity.this, CourseViewActivity.class);
                    CourseViewActivity.fromCourseView = false;
                    startActivity(intent);
                    finish();
                    return true;
                }
                else if (MainActivity.addNewCourse || MainActivity.editCourse) {
                    intent = new Intent(CourseEditActivity.this, MainActivity.class);
                    MainActivity.addNewCourse = false;
                    MainActivity.editCourse = false;
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    public void showCourseStartDatePicker(View v) {
        DatePickerFragment.datePicker = 5;
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.init(this,this);
        newFragment.show(getFragmentManager(), "courseStartPicker");
    }

    public void showCourseEndDatePicker(View v) {
        DatePickerFragment.datePicker = 6;
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.init(this,this);
        newFragment.show(getFragmentManager(), "courseEndPicker");
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        month += 1;
        String dayString;
        String monthString;
        dayString = String.format("%02d", day);
        monthString = String.format("%02d", month);
        String datePickerString = monthString + "-" + dayString
                + "-" + Integer.toString(year);
        switch(DatePickerFragment.datePicker) {
            case 5:
                cStartDate.setText(datePickerString);
                break;
            case 6:
                cEndDate.setText(datePickerString);
                break;
        }
    }

    private void saveCourse() {
        if (!courseNum.getText().toString().equals("") && !courseName.getText().toString().equals("")) {
            String newStartDate  = cStartDate.getText().toString().substring(6)
                    + "-" + cStartDate.getText().toString().substring(0, 3)
                    + cStartDate.getText().toString().substring(3, 5);
            String newEndDate = cEndDate.getText().toString().substring(6)
                    + "-" + cEndDate.getText().toString().substring(0, 3)
                    + cEndDate.getText().toString().substring(3, 5);
            db = new DatabaseMgr(this);
            int mentorFK = -1;//no mentor associated with course
            int termFK = -1;//no term associated with course
            Course course = new Course();
            Mentor mentor = new Mentor();
            course.setCourseNum(courseNum.getText().toString());
            course.setName(courseName.getText().toString());

            course.setDateStart(newStartDate);
            course.setDateEnd(newEndDate);
            course.setStatus(statusDropDown[statusSpinnerPos]);
            course.setCourseId(courseId);
            course.setNotes(courseNotes);
            course.setAlertStart(alertStart);
            course.setAlertEnd(alertEnd);
            //get mentor name from dropdown, auto populate email, phone
            //or enter new mentor name, email, phone
            //if mentor name not null, must enter email and phone
            mentor.setMentorName(mentorName.getText().toString());
            //if mentor name null, email cannot be entered
            mentor.setMentorEmail(mentorEmail.getText().toString());
            //if mentor name null, phone cannot be entered
            mentor.setMentorPhone(mentorPhone.getText().toString());

            for (Term t : Term.getTermList()) {
                if (t.getTermId() < 2) {

                    course.setTermIdFK(-1);

                } else if (t.getTermId() > 1) {//term 0 is termId 1, skip it
                    if (termDropDown.get(termSpinnerPos).equals(Integer.toString(t.getTermNumber()))) {
                        course.setTermIdFK(t.getTermId());
                        t.getAssociatedCourses().add(course);//add course to term associated course list
                        termFK = t.getTermId();
                    }
                }
                //System.out.println(termFK);
            }

            //if a new mentor was added to course, add mentor info to db

            if (!mentorName.getText().toString().equals("")//if mentor name not blank
                    && !mentorTemp.equals(mentorName.getText().toString())) {//and if new mentor name was typed
                db.insertMentor(mentorName.getText().toString(),//add mentor to list
                        mentorEmail.getText().toString(),
                        mentorPhone.getText().toString());
                //clear original mentor list.
                //need to do this to get new mentor Id from db.
                Mentor.getMentorList().clear();
                //get new mentor list to get new mentor Id to put into FK of course and populate mentor spinner
                Mentor.setMentorList(db.getMentorListFromDB());
            }

            //place new mentor Id into FK of course
            //also keeps original key if no change
            for (Mentor m : Mentor.getMentorList()) {//go through mentor list to see which name matches
                if (!mentorName.getText().toString().equals("")
                        && mentorName.getText().toString().equals(m.getMentorName())) {
                    course.setMentorIdFK(m.getId());//set the new mentor id into course
                    mentorFK = m.getId();
                }
            }

            if (MainActivity.addNewCourse || CourseListActivity.addNewCourseFromCLAct) {//add new course
                Course.getCourseList().add(course);//add course to course list
                db.insertCourse(courseName.getText().toString(),//add course to db
                        courseNum.getText().toString(),
                        statusDropDown[statusSpinnerPos],
                        newStartDate,
                        newEndDate,
                        alertStart, alertEnd, null,
                        termFK, mentorFK);
                Course.getCourseList().clear();//clear course list
                Course.setCourseList(db.getCourseListFromDB());//reload course list with changes
                MainListAdapter.courseIdKeyMatch = Course.getCourseList().get(Course.getCourseList().size() - 1).getCourseId();
            } else if (!MainActivity.addNewCourse && !CourseListActivity.addNewCourseFromCLAct) {//update course
                db.updateCourse(courseId, courseName.getText().toString(),//update existing course in db
                        courseNum.getText().toString(),
                        statusDropDown[statusSpinnerPos],
                        newStartDate,
                        newEndDate,
                        alertStart, alertEnd, courseNotes,
                        termFK, mentorFK);
                Course.getCourseList().clear();//clear course list
                Course.setCourseList(db.getCourseListFromDB());//reload course list with changes
            }
            //todo listLoad method
            Course.getCourseList().clear();//clear course list
            Course.setCourseList(db.getCourseListFromDB());//reload course list with changes
            //reload assessment lists into courses
            for (Course c : Course.getCourseList()) {//block to reload course assosciated assessments
                c.getAssociatedAssessments().clear();
                for (Assessment a : Assessment.getAssessmentList()) {
                    if (c.getCourseId() == a.getCourseIdFK()) {
                        c.getAssociatedAssessments().add(a);
                    }
                }
            }
            CourseListActivity.addNewCourseFromCLAct = false;
            CourseViewActivity.fromCourseView = false;
            //confirmation toast message
            Toast.makeText(
                    getApplicationContext(),
                    "Course saved", Toast.LENGTH_SHORT
            ).show();
            Intent intent = new Intent(this, CourseViewActivity.class);
            startActivity(intent);
            finish();
        } else {
            AlertDialog.Builder ab = new AlertDialog.Builder(this);
            ab.setTitle("Missing Fields")
                    .setMessage("Course Name & Number must be entered")
                    .setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(
                                DialogInterface dialog,
                                int yes) {
                            //do nothing
                            dialog.dismiss();
                        }
                    })
                    .create()
                    .show();
        }
    }

    private void clearFields() {
        courseNum.setText("");
        courseName.setText("");
        mentorName.setText("");
        mentorEmail.setText("");
        mentorPhone.setText("");
    }
}
