package com.example.tomjacques.coursetracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

/**
 * Created by tomjacques on 1/22/18.
 */

public class DatabaseMgr extends SQLiteOpenHelper {


    public SQLiteDatabase getDatabase() {
        SQLiteDatabase database = this.getWritableDatabase();
        return database;
    }

    //constants for db name and version
    private static final String DATABASE_NAME = "course_tracker.db";
    private static final int DATABASE_VERSION = 1;

    //constants for tables and columns go here
    public static final String TABLE_COURSE = "course";
    public static final String COURSE_ID = "_id";
    public static final String COURSE_NAME = "course_name";
    public static final String COURSE_NUM = "course_num";
    public static final String COURSE_STATUS = "course_status";
    public static final String COURSE_START = "course_start";
    public static final String COURSE_END = "course_end";
    public static final String ALERT_START = "alert_start";
    public static final String ALERT_END = "alert_end";
    public static final String COURSE_NOTES = "course_notes";
    public static final String TERM_ID_FK = "term_id_fk";
    public static final String MENTOR_ID_FK = "mentor_id_fk";

    public static final String[] ALL_COURSE_COLUMNS =
            {COURSE_ID, COURSE_NAME, COURSE_NUM, COURSE_STATUS, COURSE_START, COURSE_END,
            ALERT_START, ALERT_END, COURSE_NOTES, TERM_ID_FK, MENTOR_ID_FK};

    public static final String TABLE_ASSESS = "assessments";
    public static final String ASSESS_ID = "_id";
    public static final String ASSESS_NAME = "assess_name";
    public static final String ASSESS_TYPE = "assess_type";
    public static final String QUESTIONS = "questions";
    public static final String PASS_FAIL_CRITERIA = "pass_fail";
    public static final String MY_SCORE = "my_score";
    public static final String DUE_DATE = "due_date";
    public static final String ALERT_DATE = "alert_date";
    public static final String ALERT_GOAL = "alert_goal";
    public static final String COURSE_ID_FK = "course_notes";

    public static final String[] ALL_ASSESS_COLUMNS =
            {ASSESS_ID, ASSESS_NAME, ASSESS_TYPE, QUESTIONS, PASS_FAIL_CRITERIA,
                    MY_SCORE, DUE_DATE, ALERT_DATE, ALERT_GOAL, COURSE_ID_FK};

    public static final String TABLE_TERM = "term";
    public static final String TERM_ID = "_id";
    public static final String TERM_NUMBER = "term_num";
    public static final String TERM_NAME = "term_name";
    public static final String TERM_START = "term_start";
    public static final String TERM_END = "term_end";

    public static final String[] ALL_TERM_COLUMNS =
            {TERM_ID, TERM_NUMBER, TERM_NAME, TERM_START, TERM_END};

    public static final String TABLE_MENTOR = "mentor";
    public static final String MENTOR_ID = "_id";
    public static final String MENTOR_NAME = "mentor_name";
    public static final String MENTOR_EMAIL = "mentor_email";
    public static final String MENTOR_PHONE = "mentor_phone";

    public static final String[] ALL_MENTOR_COLUMNS =
            {MENTOR_ID, MENTOR_NAME, MENTOR_EMAIL, MENTOR_PHONE};


    //SQL to create tables goes here
    private static final String COURSE_TABLE_CREATE =
            "CREATE TABLE " + TABLE_COURSE + " (" +
                    COURSE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COURSE_NAME + " TEXT, " +
                    COURSE_NUM + " TEXT, " +
                    COURSE_STATUS + " TEXT, " +
                    COURSE_START + " TEXT, " +
                    COURSE_END + " TEXT, " +
                    ALERT_START + " INTEGER, " +
                    ALERT_END + " INTEGER, " +
                    COURSE_NOTES + " TEXT, " +
                    TERM_ID_FK + " INTEGER, " +
                    MENTOR_ID_FK + " INTEGER" +
                    ")";

    private static final String ASSESS_TABLE_CREATE =
            "CREATE TABLE " + TABLE_ASSESS + " (" +
                    ASSESS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    ASSESS_NAME + " TEXT, " +
                    ASSESS_TYPE + " TEXT, " +
                    QUESTIONS + " INTEGER, " +
                    PASS_FAIL_CRITERIA + " TEXT, " +
                    MY_SCORE + " TEXT, " +
                    DUE_DATE + " TEXT, " +
                    ALERT_DATE + " TEXT, " +
                    ALERT_GOAL + " INTEGER, " +
                    COURSE_ID_FK + " INTEGER" +
                    ")";

    private static final String TERM_TABLE_CREATE =
            "CREATE TABLE " + TABLE_TERM + " (" +
                    TERM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    TERM_NUMBER + " INTEGER, " +
                    TERM_NAME + " TEXT, " +
                    TERM_START + " TEXT, " +
                    TERM_END + " TEXT" +
                    ")";

    private static final String MENTOR_TABLE_CREATE =
            "CREATE TABLE " + TABLE_MENTOR + " (" +
                    MENTOR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    MENTOR_NAME + " TEXT, " +
                    MENTOR_PHONE + " TEXT, " +
                    MENTOR_EMAIL + " TEXT" +
                    ")";


    public DatabaseMgr(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //add tables here
        sqLiteDatabase.execSQL(COURSE_TABLE_CREATE);
        sqLiteDatabase.execSQL(ASSESS_TABLE_CREATE);
        sqLiteDatabase.execSQL(TERM_TABLE_CREATE);
        sqLiteDatabase.execSQL(MENTOR_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_COURSE);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSESS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_TERM);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_MENTOR);
        onCreate(sqLiteDatabase);
    }

    public List<Course> getCourseListFromDB() {
        Cursor cursor = getDatabase().query(DatabaseMgr.TABLE_COURSE, ALL_COURSE_COLUMNS,
                null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Course course = new Course();
            course.setCourseId(cursor.getInt(0));
            course.setName(cursor.getString(1));
            course.setCourseNum(cursor.getString(2));
            course.setStatus(cursor.getString(3));
            course.setDateStart(cursor.getString(4));
            course.setDateEnd(cursor.getString(5));
            course.setAlertStart(cursor.getInt(6));
            course.setAlertEnd(cursor.getInt(7));
            course.setNotes(cursor.getString(8));
            course.setTermIdFK(cursor.getInt(9));
            course.setMentorIdFK(cursor.getInt(10));
            Course.getCourseList().add(course);
            cursor.moveToNext();
        }
        cursor.close();
        return Course.getCourseList();
    }

    public void insertCourse(String name, String courseNum, String status, String courseStart, String courseEnd,
                             int alertStart, int alertEnd, String notes, int termIdFK, int mentorIdFK) {
        ContentValues values = new ContentValues();
        values.put(DatabaseMgr.COURSE_NAME, name);
        values.put(DatabaseMgr.COURSE_NUM, courseNum);
        values.put(DatabaseMgr.COURSE_STATUS, status);
        values.put(DatabaseMgr.COURSE_START, courseStart);
        values.put(DatabaseMgr.COURSE_END, courseEnd);
        values.put(DatabaseMgr.ALERT_START, alertStart);
        values.put(DatabaseMgr.ALERT_END, alertEnd);
        values.put(DatabaseMgr.COURSE_NOTES, notes);
        values.put(DatabaseMgr.TERM_ID_FK, termIdFK);
        values.put(DatabaseMgr.MENTOR_ID_FK, mentorIdFK);
        getDatabase().insert(DatabaseMgr.TABLE_COURSE, null, values);
        getDatabase().close();
    }

    public void updateCourse(int courseId, String name, String courseNum, String status, String courseStart, String courseEnd,
                             int alertStart, int alertEnd, String notes, int termIdFK, int mentorIdFK) {
        ContentValues values = new ContentValues();
        values.put(DatabaseMgr.COURSE_NAME, name);
        values.put(DatabaseMgr.COURSE_NUM, courseNum);
        values.put(DatabaseMgr.COURSE_STATUS, status);
        values.put(DatabaseMgr.COURSE_START, courseStart);
        values.put(DatabaseMgr.COURSE_END, courseEnd);
        values.put(DatabaseMgr.ALERT_START, alertStart);
        values.put(DatabaseMgr.ALERT_END, alertEnd);
        values.put(DatabaseMgr.COURSE_NOTES, notes);
        values.put(DatabaseMgr.TERM_ID_FK, termIdFK);
        values.put(DatabaseMgr.MENTOR_ID_FK, mentorIdFK);
        getDatabase().update(DatabaseMgr.TABLE_COURSE, values, COURSE_ID + "=" + courseId, null);
        getDatabase().close();
    }

    public void updateCourseAlertStart(int courseId, int alertStart) {
        ContentValues values = new ContentValues();
        values.put(DatabaseMgr.ALERT_START, alertStart);
        getDatabase().update(DatabaseMgr.TABLE_COURSE, values, COURSE_ID + "=" + courseId, null);
        getDatabase().close();
    }

    public void updateCourseAlertEnd(int courseId, int alertEnd) {
        ContentValues values = new ContentValues();
        values.put(DatabaseMgr.ALERT_END, alertEnd);
        getDatabase().update(DatabaseMgr.TABLE_COURSE, values, COURSE_ID + "=" + courseId, null);
        getDatabase().close();
    }

    public void dBDropCourse(int courseId, String status, String courseStart, String courseEnd, int alertStart, int alertEnd,
                             int termIdFK) {
        ContentValues values = new ContentValues();
        values.put(DatabaseMgr.COURSE_STATUS, status);
        values.put(DatabaseMgr.COURSE_START, courseStart);
        values.put(DatabaseMgr.COURSE_END, courseEnd);
        values.put(DatabaseMgr.ALERT_START, alertStart);
        values.put(DatabaseMgr.ALERT_END, alertEnd);
        values.put(DatabaseMgr.TERM_ID_FK, termIdFK);
        getDatabase().update(DatabaseMgr.TABLE_COURSE, values, COURSE_ID + "=" + courseId, null);
        getDatabase().close();
    }

    public void deleteAllCoursesFromTable() {
        getDatabase().delete(DatabaseMgr.TABLE_COURSE, null, null);
        getDatabase().close();
    }

    public void deleteCourseFromDb(int courseId) {
        getDatabase().delete(DatabaseMgr.TABLE_COURSE, COURSE_ID + "=" + courseId, null);
        getDatabase().close();
    }

    public List<Assessment> getAssessmentListFromDB() {
        Cursor cursor = getDatabase().query(DatabaseMgr.TABLE_ASSESS, ALL_ASSESS_COLUMNS,
                null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Assessment assessment = new Assessment();
            assessment.setAssessId(cursor.getInt(0));
            assessment.setAssessName(cursor.getString(1));
            assessment.setAssessType(cursor.getString(2));
            assessment.setQuestions(cursor.getInt(3));
            assessment.setPassFailCriteria(cursor.getString(4));
            assessment.setMyScore(cursor.getString(5));
            assessment.setDueDate(cursor.getString(6));
            assessment.setGoalDate(cursor.getString(7));
            assessment.setGoalDateAlert(cursor.getInt(8));
            assessment.setCourseIdFK(cursor.getInt(9));
            Assessment.getAssessmentList().add(assessment);
            cursor.moveToNext();
        }
        cursor.close();
        //database.close();
        return Assessment.getAssessmentList();
    }

    public void insertAssessment(String name, String type, int questions, String passFail,
                                 String myScore, String dueDate, String alertDate, int goalAlert, int courseIdFK) {
        ContentValues values = new ContentValues();
        values.put(DatabaseMgr.ASSESS_NAME, name);
        values.put(DatabaseMgr.ASSESS_TYPE, type);
        values.put(DatabaseMgr.QUESTIONS, questions);
        values.put(DatabaseMgr.PASS_FAIL_CRITERIA, passFail);
        values.put(DatabaseMgr.MY_SCORE, myScore);
        values.put(DatabaseMgr.DUE_DATE, dueDate);
        values.put(DatabaseMgr.ALERT_DATE, alertDate);
        values.put(DatabaseMgr.ALERT_GOAL, goalAlert);
        values.put(DatabaseMgr.COURSE_ID_FK, courseIdFK);
        getDatabase().insert(DatabaseMgr.TABLE_ASSESS, null, values);
        getDatabase().close();
    }

    public void updateAssessment(int assessId, String name, String type, int questions, String passFail,
                                 String myScore, String dueDate, String alertDate, int goalAlert, int courseIdFK) {
        ContentValues values = new ContentValues();
        values.put(DatabaseMgr.ASSESS_NAME, name);
        values.put(DatabaseMgr.ASSESS_TYPE, type);
        values.put(DatabaseMgr.QUESTIONS, questions);
        values.put(DatabaseMgr.PASS_FAIL_CRITERIA, passFail);
        values.put(DatabaseMgr.MY_SCORE, myScore);
        values.put(DatabaseMgr.DUE_DATE, dueDate);
        values.put(DatabaseMgr.ALERT_DATE, alertDate);
        values.put(DatabaseMgr.ALERT_GOAL, goalAlert);
        values.put(DatabaseMgr.COURSE_ID_FK, courseIdFK);
        getDatabase().update(DatabaseMgr.TABLE_ASSESS, values, ASSESS_ID + "=" + assessId, null);
        getDatabase().close();
    }

    public void updateAssessmentAlert(int assessId, int goalAlert) {
        ContentValues values = new ContentValues();
        values.put(DatabaseMgr.ALERT_GOAL, goalAlert);
        getDatabase().update(DatabaseMgr.TABLE_ASSESS, values, ASSESS_ID + "=" + assessId, null);
        getDatabase().close();
    }

    public void deleteAssessmentFromDb(int assessId) {
        getDatabase().delete(DatabaseMgr.TABLE_ASSESS, ASSESS_ID + "=" + assessId, null);
        getDatabase().close();
    }

    public List<Term> getTermListFromDB() {
        Cursor cursor = getDatabase().query(DatabaseMgr.TABLE_TERM, ALL_TERM_COLUMNS,
                null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Term term = new Term();
            term.setTermId(cursor.getInt(0));
            term.setTermNumber(cursor.getInt(1));
            term.setTermName(cursor.getString(2));
            term.setTermStart(cursor.getString(3));
            term.setTermEnd(cursor.getString(4));
            Term.getTermList().add(term);
            cursor.moveToNext();
        }
        cursor.close();
        return Term.getTermList();
    }

    public void insertTerm(int termNum, String termName, String termStart, String termEnd) {
        ContentValues values = new ContentValues();
        values.put(DatabaseMgr.TERM_NUMBER, termNum);
        values.put(DatabaseMgr.TERM_NAME, termName);
        values.put(DatabaseMgr.TERM_START, termStart);
        values.put(DatabaseMgr.TERM_END, termEnd);
        getDatabase().insert(DatabaseMgr.TABLE_TERM, null, values);
        getDatabase().close();
    }

    public void updateTerm(int termId, int termNum, String termName, String termStart, String termEnd) {
        ContentValues values = new ContentValues();
        values.put(DatabaseMgr.TERM_NUMBER, termNum);
        values.put(DatabaseMgr.TERM_NAME, termName);
        values.put(DatabaseMgr.TERM_START, termStart);
        values.put(DatabaseMgr.TERM_END, termEnd);
        getDatabase().update(DatabaseMgr.TABLE_TERM, values, TERM_ID + "=" + termId, null);
        getDatabase().close();
    }

    public void updateTermNumber(int termId, int termNum) {
        ContentValues values = new ContentValues();
        values.put(DatabaseMgr.TERM_NUMBER, termNum);
        getDatabase().update(DatabaseMgr.TABLE_TERM, values, TERM_ID + "=" + termId, null);
        getDatabase().close();
    }

    public void deleteTermFromDb(int termId) {
        getDatabase().delete(DatabaseMgr.TABLE_TERM, TERM_ID + "=" + termId, null);
        getDatabase().close();
    }

    public List<Mentor> getMentorListFromDB() {
        Cursor cursor = getDatabase().query(DatabaseMgr.TABLE_MENTOR, ALL_MENTOR_COLUMNS,
                null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Mentor mentor = new Mentor();
            mentor.setId(cursor.getInt(0));
            mentor.setMentorName(cursor.getString(1));
            mentor.setMentorEmail(cursor.getString(2));
            mentor.setMentorPhone(cursor.getString(3));
            Mentor.getMentorList().add(mentor);
            cursor.moveToNext();
        }
        cursor.close();
        return Mentor.getMentorList();
    }

    public void deleteMentorFromTable(int mentorId) {
        getDatabase().delete(DatabaseMgr.TABLE_MENTOR, MENTOR_ID + "=" + mentorId, null);
        getDatabase().close();
    }

    public void insertMentor(String mentName, String mentEmail, String mentPhone) {
        ContentValues values = new ContentValues();
        values.put(DatabaseMgr.MENTOR_NAME, mentName);
        values.put(DatabaseMgr.MENTOR_EMAIL, mentEmail);
        values.put(DatabaseMgr.MENTOR_PHONE, mentPhone);
        getDatabase().insert(DatabaseMgr.TABLE_MENTOR, null, values);
        getDatabase().close();
    }
}


