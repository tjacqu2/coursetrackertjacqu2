package com.example.tomjacques.coursetracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * Created by tomjacques on 1/13/18.
 */

public class TermViewActivity extends AppCompatActivity {// implements OnTouchListener
    public static boolean color = false;
    DatabaseMgr db;
    public AlertDialog.Builder ad;
    public static boolean fromTermView = false;
    private static int termIdDelete;
    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    LinkedHashMap<String, List<String>> expandableListDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TermViewActivity.color = true;
        expandableListView = (ExpandableListView) findViewById(R.id.expandable_singleTermlist_view);
        expandableListAdapter = new ExpandableTermListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListDetail = ExpandableTermDataRetrieve.getSingleTermData((int)expandableListAdapter.getGroupId(TermListActivity.termListPos));
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new ExpandableTermListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, final int i, long l) {
                PopupMenu popup = new PopupMenu(TermViewActivity.this, view);
                popup.getMenuInflater().inflate(R.menu.popup_menu_termview_term,
                        popup.getMenu());
                popup.show();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_add_course_termview:
                                Intent intent = new Intent(TermViewActivity.this, CourseListActivity.class);
                                startActivity(intent);
                                return true;
                            case R.id.action_delete_term_termview:
                                if (deleteTerm()) {
                                    deleteAlert();
                                } else {
                                    termNotEmptyAlert();
                                }
                                return true;
                            default:
                                return false;
                        }

                    }
                });
                return true;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                TermViewActivity.color = false;
                MainListAdapter.courseIdKeyMatch = ExpandableTermDataRetrieve.expandableCourseList.get(i1);
                PopupMenu popup = new PopupMenu(TermViewActivity.this, view);
                popup.getMenuInflater().inflate(R.menu.popup_menu_termview_course,
                        popup.getMenu());
                popup.show();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_show_details_termview:
                                Intent intent = new Intent(TermViewActivity.this, CourseViewActivity.class);
                                startActivity(intent);
                                return true;
                            case R.id.action_assessment_termview:
                                intent = new Intent(TermViewActivity.this, AssessmentListActivity.class);
                                startActivity(intent);
                                return true;
                            case R.id.action_notes_termview:
                                intent = new Intent(TermViewActivity.this, NotesViewActivity.class);
                                startActivity(intent);
                                return true;
                            case R.id.drop_course_termview:
                                dropCourse(MainListAdapter.courseIdKeyMatch);
                                intent = new Intent(TermViewActivity.this, TermViewActivity.class);
                                startActivity(intent);
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        expandableListAdapter = new ExpandableTermListAdapter(this, expandableListTitle, expandableListDetail);
        int count = expandableListAdapter.getGroupCount();
        for(int i = 0; i<count; ++i) {
            expandableListView.expandGroup(i);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.term_view_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml
        Intent intent;
        switch (item.getItemId()) {
        case android.R.id.home:
            TermViewActivity.color = false;
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return true;
        case R.id.action_edit:
            TermViewActivity.color = false;
            fromTermView = true;
            intent = new Intent(this, TermEditActivity.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TermViewActivity.color = false;
    }

    public boolean deleteTerm() {
        boolean delete = false;
        int count = 0;//-1
        for (Term t : Term.getTermList()) {
            if (t.getTermId() > 1) {// todo term
                if (TermListActivity.termListPos == count) {//specifies the clicked term
                    if (t.getAssociatedCourses().isEmpty()) {//if that term is empty
                        delete = true;
                        termIdDelete = t.getTermId();
                    }
                }
            }
            count++;
        }
        return delete;
    }

    private void deleteAlert() {
        db = new DatabaseMgr(this);
        ad = new AlertDialog.Builder(this);
        ad.setTitle("Confirm Delete")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog,
                                    int yes) {
                                //delete term
                                db.deleteTermFromDb(termIdDelete);
                                Term.getTermList().clear();
                                Term.setTermList(db.getTermListFromDB());

                                for (Term t : Term.getTermList()) {
                                    if (t.getTermId() > termIdDelete) {
                                        t.setTermNumber(t.getTermNumber() - 1);
                                        db.updateTermNumber(t.getTermId(), t.getTermNumber());
                                    }
                                }

                                for (Term t : Term.getTermList()) {
                                    t.getAssociatedCourses().clear();
                                    for (Course c : Course.getCourseList()) {
                                        if (c.getTermIdFK() == t.getTermId()) {
                                            t.getAssociatedCourses().add(c);
                                        }
                                    }
                                }
                                Intent intent = new Intent(TermViewActivity.this, TermListActivity.class);
                                startActivity(intent);
                                TermViewActivity.color = false;
                                Toast.makeText(
                                        getApplicationContext(),
                                        "Term deleted", Toast.LENGTH_SHORT
                                ).show();
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog,
                                    int no) {
                                //do nothing
                                dialog.dismiss();
                            }
                        })
                .setCancelable(false)
                .create()
                .show();
    }

    private void termNotEmptyAlert() {
        ad = new AlertDialog.Builder(this);
        ad.setTitle("Term Is Not Empty")
                .setMessage("Term can't be deleted")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(
                            DialogInterface dialog,
                            int ok) {
                        //do nothing
                        dialog.dismiss();
                    }

                })
                .create()
                .show();

    }

    public void dropCourse(int courseId) {
        db = new DatabaseMgr(this);
        for (Course c : Course.getCourseList()) {
            if (courseId == c.getCourseId()) {
                db.updateCourse(courseId,
                        c.getName(),
                        c.getCourseNum(),
                        "Dropped",
                        "yyyy-MM-dd",
                        "yyyy-MM-dd",
                        0,
                        0,
                        c.getNotes(),
                        -1,
                        c.getMentorIdFK());

            }
        }
        //todo listLoad method
        Course.getCourseList().clear();//clear course list
        Course.setCourseList(db.getCourseListFromDB());//reload course list with changes
        for (Course c : Course.getCourseList()) {//block to reload course assosciated assessments
            c.getAssociatedAssessments().clear();
            for (Assessment a : Assessment.getAssessmentList()) {
                if (c.getCourseId() == a.getCourseIdFK()) {
                    c.getAssociatedAssessments().add(a);
                }
            }
        }
        Term.getTermList().clear();
        Term.setTermList(db.getTermListFromDB());
        for (Term t : Term.getTermList()) {
            t.getAssociatedCourses().clear();
            for (Course c : Course.getCourseList()) {
                if (c.getTermIdFK() == t.getTermId()) {
                    t.getAssociatedCourses().add(c);
                }
            }
        }
        Toast.makeText(
                getApplicationContext(),
                "Course dropped", Toast.LENGTH_SHORT
        ).show();
    }
}
