package com.example.tomjacques.coursetracker;

import android.graphics.Color;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MLAdapterInterface {//OnTouchListener

    MainListAdapter myMainListAdapter;
    TextView thisTerm;
    DatabaseMgr db;//
    BroadcastReceiver rec;
    public static boolean addNewCourse = false;
    public static boolean addNewTermFromMainAct = false;
    public static boolean editCourse = false;
    public ArrayList<HashMap<String, String>> listMain;
    public static final String FIRST_COL = "first";
    public static final String SECOND_COL = "second";
    public static final String ID_KEY = "third";
    public static final String ALERT_START = "alertStart";
    public static final String ALERT_END = "alertEnd";
    public static final String STATUS = "status";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        addNewCourse = false;
        boolean currentTerm = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(android.R.id.list);
        listMain = new ArrayList<>();
        db = new DatabaseMgr(this);
        //clear lists so they aren't reloaded to screen
        Course.getCourseList().clear();
        Mentor.getMentorList().clear();
        Term.getTermList().clear();
        Assessment.getAssessmentList().clear();

        //reload data from database into lists
        Course.setCourseList(db.getCourseListFromDB());
        Mentor.setMentorList(db.getMentorListFromDB());
        Term.setTermList(db.getTermListFromDB());
        Assessment.setAssessmentList(db.getAssessmentListFromDB());

        //Load mentor db table with an initial default "Select Mentor" for mentor spinner
        if (Mentor.getMentorList().isEmpty()) {
            db.insertMentor("Select Mentor", "", "");
        }
        //Load term list with an initial default term that will be 0 term for term spinner
        if (Term.getTermList().isEmpty()) {
            db.insertTerm(0, "", "", "");
        }

        //clear lists
        Course.getCourseList().clear();
        Mentor.getMentorList().clear();
        Term.getTermList().clear();
        Assessment.getAssessmentList().clear();

        //reload data from database into lists
        Course.setCourseList(db.getCourseListFromDB());
        Mentor.setMentorList(db.getMentorListFromDB());
        Term.setTermList(db.getTermListFromDB());
        Assessment.setAssessmentList(db.getAssessmentListFromDB());
        //set associated courses into terms
        //todo listLoad method
        for (Term t : Term.getTermList()) {
            if (t.getTermNumber() != 0) {
                for (Course c : Course.getCourseList()) {
                    if (t.getTermId() == c.getTermIdFK()) {
                        t.getAssociatedCourses().add(c);
                    }
                }
            }
        }
        //todo listLoad method
        //set associated assessments into courses
        for (Course c : Course.getCourseList()) {
            c.getAssociatedAssessments().clear();
            for (Assessment a : Assessment.getAssessmentList()) {
                if (c.getCourseId() == a.getCourseIdFK()) {
                    c.getAssociatedAssessments().add(a);
                }
            }
        }
        //put data in a hash map to display in columns on screen
        Date cal = Calendar.getInstance().getTime();

        //CharSequence date = DateFormat.format("yyyy-MM-dd", cal);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateNow = sdf.format(cal);
        int count = 0;
        String nameCheck;
        for (Term t : Term.getTermList()) {
            if (t.getTermId() > 0) {//was 1 todo term
                if(t.getTermName().equals("")) {
                    nameCheck = "";
                } else {
                    nameCheck = " (" + t.getTermName() + ")";
                }
                try {
                    if (sdf.parse(dateNow).before(sdf.parse(t.getTermEnd()))
                            && sdf.parse(dateNow).after(sdf.parse(t.getTermStart()))) {
                        String dateTerm = "Current Term: Term " + Integer.toString(t.getTermNumber())
                                            + nameCheck;
                        thisTerm = findViewById(R.id.currentTerm);
                        thisTerm.setText(dateTerm);
                        TermListActivity.termListPos = count;
                        for (Course c : t.getAssociatedCourses()) {
                            HashMap<String, String> temp = new HashMap<>();
                            temp.put(FIRST_COL, c.getName());
                            temp.put(SECOND_COL, c.getCourseNum());
                            temp.put(ID_KEY, Integer.toString(c.getCourseId()));
                            temp.put(ALERT_START, Integer.toString(c.getAlertStart()));
                            temp.put(ALERT_END, Integer.toString(c.getAlertEnd()));
                            temp.put(STATUS, c.getStatus());
                            listMain.add(temp);
                        }
                        currentTerm = true;
                        break;
                    } else {
                        String notFound = "Current Term: No current term found";
                        thisTerm = findViewById(R.id.currentTerm);
                        thisTerm.setText(notFound);
                    }
                } catch (Exception e) {
                    //do nothing
                }
            }
            ++count;
        }
        if (!currentTerm) {
            String notFound = "Current Term: No current term found";
            thisTerm = findViewById(R.id.currentTerm);
            thisTerm.setText(notFound);
        }
        //display list
        myMainListAdapter = new MainListAdapter(this, listMain, this);

        listView.setAdapter(myMainListAdapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (toolbar != null) {
            toolbar.setLogo(R.mipmap.ic_fa_graduation_cap);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onResume() {
        super.onResume();
        checkAlerts();
        rec = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                checkAlerts();
            }
        };
        IntentFilter intFilt = new IntentFilter(
                Intent.ACTION_DATE_CHANGED);
        this.registerReceiver(rec, intFilt);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        SpannableStringBuilder ssb = new SpannableStringBuilder();

        String sNameTitle = "Student: ";
        SpannableString student = new SpannableString(sNameTitle);
        student.setSpan(new ForegroundColorSpan(Color.BLACK),0, sNameTitle.length(), 0);
        ssb.append(student);

        String studentName = sp.getString(SettingsActivity.STUDENT_NAME, "");
        SpannableString studColor = new SpannableString(studentName);
        studColor.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue)), 0, studentName.length(), 0);
        ssb.append(studColor);
        TextView sName = findViewById(R.id.perProfileName);
        sName.setText(ssb, TextView.BufferType.SPANNABLE);

        SpannableStringBuilder ssb2 = new SpannableStringBuilder();

        String sIdTitle = "Student ID: ";
        SpannableString studId = new SpannableString(sIdTitle);
        studId.setSpan(new ForegroundColorSpan(Color.BLACK),0, sIdTitle.length(), 0);
        ssb2.append(studId);

        String studentId = sp.getString(SettingsActivity.STUDENT_ID, "");
        SpannableString iDColor = new SpannableString(studentId);
        iDColor.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue)), 0, studentId.length(), 0);
        ssb2.append(iDColor);
        TextView sId = findViewById(R.id.perProfileId);
        sId.setText(ssb2, TextView.BufferType.SPANNABLE);

        SpannableStringBuilder ssb3 = new SpannableStringBuilder();

        String instTitle = "Institution: ";
        SpannableString institution = new SpannableString(instTitle);
        institution.setSpan(new ForegroundColorSpan(Color.BLACK),0, instTitle.length(), 0);
        ssb3.append(institution);

        String schoolName = sp.getString(SettingsActivity.SCHOOL_NAME, "");
        SpannableString instColor = new SpannableString(schoolName);
        instColor.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue)), 0, schoolName.length(), 0);
        ssb3.append(instColor);
        TextView schName = findViewById(R.id.schoolProfileName);
        schName.setText(ssb3, TextView.BufferType.SPANNABLE);

        SpannableStringBuilder ssb4 = new SpannableStringBuilder();

        String degreeTitle = "Degree: ";
        SpannableString degree = new SpannableString(degreeTitle);
        degree.setSpan(new ForegroundColorSpan(Color.BLACK),0, degreeTitle.length(), 0);
        ssb4.append(degree);

        String degreeProgram = sp.getString(SettingsActivity.DEGREE_PROGRAM, "");
        SpannableString degreeColor = new SpannableString(degreeProgram);
        degreeColor.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue)), 0, degreeProgram.length(), 0);
        ssb4.append(degreeColor);
        TextView schDp = findViewById(R.id.schoolProfileDp);
        schDp.setText(ssb4, TextView.BufferType.SPANNABLE);
    }

    @Override
    protected void onDestroy() {
        if (rec != null) {
            unregisterReceiver(rec);
            rec = null;
        }
        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void checkAlerts() {
        long vibrate[] = {0,1000, 0};
        Calendar cal = Calendar.getInstance();
        CharSequence date = DateFormat.format("yyyy-MM-dd", cal);
        List<Notification> notifications = new ArrayList<>();
        for (Course c : Course.getCourseList()) {
            if (date.equals(c.getDateStart()) && c.getAlertStart() == 1) { //&& alertStart == true
                Notification notification = new Notification.Builder(this)
                        .setContentTitle("New notification")
                        .setContentText("Your class, " + c.getName() + " " + c.getCourseNum() + ", starts today")
                        .setSmallIcon(android.R.drawable.star_on)
                        .setAutoCancel(true)
                        .setVibrate(vibrate)
                        .build();

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(
                                NOTIFICATION_SERVICE);
                notificationManager.notify(c.getCourseId(), notification);
                notifications.add(notification);
                c.setAlertStart(0);
                db.updateCourseAlertStart(c.getCourseId(), c.getAlertStart());
            } else if (date.equals(c.getDateEnd()) && c.getAlertEnd() == 1) {
                Notification notification = new Notification.Builder(this)
                        .setContentTitle("New notification")
                        .setContentText("Your class, " + c.getName() + " " + c.getCourseNum() + ", ends today")
                        .setSmallIcon(android.R.drawable.star_on)
                        .setVibrate(vibrate)
                        .setAutoCancel(true)
                        .build();
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(
                                NOTIFICATION_SERVICE);
                notificationManager.notify(c.getCourseId(), notification);
                notifications.add(notification);
                c.setAlertEnd(0);
                db.updateCourseAlertEnd(c.getCourseId(), c.getAlertEnd());
            }
            for (Assessment a : Assessment.getAssessmentList()) {
                if (date.equals(a.getGoalDate()) && a.getGoalDateAlert() == 1) {
                    Notification notification = new Notification.Builder(this)
                            .setContentTitle("New notification")
                            .setContentText("Your goal was to take the " + c.getName() + " " + c.getCourseNum() + " " + a.getAssessName() + " by today")
                            .setSmallIcon(android.R.drawable.star_on)
                            .setVibrate(vibrate)
                            .setAutoCancel(true)
                            .build();
                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(
                                    NOTIFICATION_SERVICE);
                    notificationManager.notify(a.getAssessId(), notification);
                    notifications.add(notification);
                    a.setGoalDateAlert(0);
                    db.updateAssessmentAlert(a.getAssessId(), a.getGoalDateAlert());

                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.term_list) {
            //handles opening the term list screen
            startActivity(new Intent(this,
                    TermListActivity.class));
        } else if (id == R.id.course_list) {
            //handles opening the course list screen
            TermListActivity.termListPos = -1;
            startActivity(new Intent(this,
                    CourseListActivity.class));
        } else if (id == R.id.add_new_courses) {
            addNewCourse = true;
            //handles add new course to list, opens empty course view screen
            startActivity(new Intent(this,
                    CourseEditActivity.class));
        } else if (id == R.id.add_new_terms) {
            addNewTermFromMainAct = true;
            startActivity(new Intent(this,
                    TermEditActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void dropCourse(int courseId) {
        db = new DatabaseMgr(this);
        for (Course c : Course.getCourseList()) {
            if (courseId == c.getCourseId()) {
                db.updateCourse(courseId,
                        c.getName(),
                        c.getCourseNum(),
                        "Dropped",
                        "yyyy-MM-dd",
                        "yyyy-MM-dd",
                        0,
                        0,
                        c.getNotes(),
                        -1,
                        c.getMentorIdFK());

            }
        }
        Course.getCourseList().clear();//clear course list
        Course.setCourseList(db.getCourseListFromDB());//reload course list with changes
        //todo listLoad method
        for (Course c : Course.getCourseList()) {//block to reload course assosciated assessments
            c.getAssociatedAssessments().clear();
            for (Assessment a : Assessment.getAssessmentList()) {
                if (c.getCourseId() == a.getCourseIdFK()) {
                    c.getAssociatedAssessments().add(a);
                }
            }
        }
        Toast.makeText(
                getApplicationContext(),
                "Course dropped", Toast.LENGTH_SHORT
        ).show();
    }
}
