package com.example.tomjacques.coursetracker;

/**
 * Created by tomjacques on 3/14/18.
 * Needed to be implemented in order to access MainListActivity
 * methods from the MainListAdapter
 */

public interface MLAdapterInterface {
    void dropCourse(int courseId);
}
