package com.example.tomjacques.coursetracker;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class NotesViewActivity extends AppCompatActivity {
    private String sendNotes;
    private String courseName;
    private String courseNum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView notes = findViewById(R.id.notesText);
        for (Course c : Course.getCourseList()) {
            if (MainListAdapter.courseIdKeyMatch == c.getCourseId()) {
                notes.setText(c.getNotes());
                sendNotes = c.getNotes();
                courseName = c.getName();
                courseNum = c.getCourseNum();
            }
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NotesViewActivity.this, NotesAddActivity.class);
                startActivity(intent);
                finish();
            }
        });

        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab_share);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, courseName + " " + courseNum + " Notes:");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, sendNotes);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                    intent = new Intent(this, CourseViewActivity.class);
                    startActivity(intent);
                    finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
