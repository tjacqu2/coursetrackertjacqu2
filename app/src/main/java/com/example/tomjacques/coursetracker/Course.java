package com.example.tomjacques.coursetracker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomjacques on 1/20/18.
 */

public class Course {

    private int courseId;
    private String name;
    private String courseNum;
    private String dateStart;
    private String dateEnd;
    private String status;
    private int alertStart;
    private int alertEnd;
    private String notes;
    private int mentorIdFK;
    private int termIdFK;
    private final List<Assessment> associatedAssessments = new ArrayList<>();
    private static List<Course> courseList = new ArrayList<>();

    public Course() {

    }

    public Course(String name, String courseNum) {
        this.name = name;
        this.courseNum = courseNum;
    }

    public Course(int courseId, String name, String courseNum, String status, String dateStart, String dateEnd, int alertStart,
                  int alertEnd, String notes, int mentorIdFK, int termIdFK, Assessment associatedAssessments) {
        this.courseId = courseId;
        this.name = name;
        this.courseNum = courseNum;
        this.status = status;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.alertStart = alertStart;
        this.alertEnd = alertEnd;
        this.notes = notes;
        this.mentorIdFK = mentorIdFK;
        this.termIdFK = termIdFK;
        this.associatedAssessments.add(associatedAssessments);
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getCourseNum() {
        return courseNum;
    }

    public void setCourseNum(String courseNum) {
        this.courseNum = courseNum;
    }

    public void setDateStart(String dateStart) {this.dateStart = dateStart;}

    public String getDateStart() {
        return dateStart;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setAlertStart(int alertStart) {
        this.alertStart = alertStart;
    }

    public int getAlertStart() {
        return alertStart;
    }

    public void setAlertEnd(int alertEnd) {
        this.alertEnd = alertEnd;
    }

    public int getAlertEnd() {
        return alertEnd;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public int getMentorIdFK() {
        return mentorIdFK;
    }

    public void setMentorIdFK(int mentorIdFK) {
        this.mentorIdFK = mentorIdFK;
    }

    public int getTermIdFK() {
        return termIdFK;
    }

    public void setTermIdFK(int termIdFK) {
        this.termIdFK = termIdFK;
    }

    public List<Assessment> getAssociatedAssessments() {
        return associatedAssessments;
    }

    public static List<Course> getCourseList() {
        return courseList;
    }

    public static void setCourseList(List<Course> courseList) {
        Course.courseList = courseList;
    }
}
