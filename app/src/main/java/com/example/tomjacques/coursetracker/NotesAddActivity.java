package com.example.tomjacques.coursetracker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class NotesAddActivity extends AppCompatActivity {
    DatabaseMgr db;
    EditText notesEditTemp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_add);
        notesEditTemp = findViewById(R.id.notesEdit);
        for (Course c : Course.getCourseList()) {
            if (MainListAdapter.courseIdKeyMatch == c.getCourseId()) {
                notesEditTemp.setText(c.getNotes());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notes_edit_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_save_notes:
                saveNotes();
                Toast.makeText(
                        getApplicationContext(),
                        "Notes saved", Toast.LENGTH_SHORT
                ).show();
                intent = new Intent(this, NotesViewActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_cancel_notes:
                intent = new Intent(this, NotesViewActivity.class);
                startActivity(intent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void saveNotes() {
        db = new DatabaseMgr(this);
        for (Course c : Course.getCourseList()) {
            if (MainListAdapter.courseIdKeyMatch == c.getCourseId()) {
                c.setNotes(notesEditTemp.getText().toString());
                db.updateCourse(c.getCourseId(),
                        c.getName(),
                    c.getCourseNum(),
                    c.getStatus(),
                    c.getDateStart(),
                    c.getDateEnd(),
                    c.getAlertStart(), c.getAlertEnd(), c.getNotes(),
                    c.getTermIdFK(), c.getMentorIdFK());
            }
        }
        Course.getCourseList().clear();//clear course list
        Course.setCourseList(db.getCourseListFromDB());//reload course list with changes
        //todo listLoad method
        for (Course c : Course.getCourseList()) {//block to reload course assosciated assessments
            c.getAssociatedAssessments().clear();
            for (Assessment a : Assessment.getAssessmentList()) {
                if (c.getCourseId() == a.getCourseIdFK()) {
                    c.getAssociatedAssessments().add(a);
                }
            }
        }
    }
}
