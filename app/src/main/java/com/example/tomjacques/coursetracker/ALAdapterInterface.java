package com.example.tomjacques.coursetracker;

/**
 * Created by tomjacques on 3/13/18.
 * Needed to be implemented in order to access AssessmentListActivity
 * methods from the AssessmentListAdapter
 */

public interface ALAdapterInterface {
    void deleteAssessment();
}
