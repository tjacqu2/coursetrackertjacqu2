package com.example.tomjacques.coursetracker;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.PopupMenu;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by tomjacques on 1/13/18.
 */

public class TermListActivity extends AppCompatActivity {
    DatabaseMgr db;
    public AlertDialog.Builder ad;
    public static boolean addNewTermFromTLAct;
    public static boolean fromTermList = false;
    public static int termListPos;
    private static int termIdDelete;
    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    LinkedHashMap<String, List<String>> expandableListDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        expandableListView = (ExpandableListView) findViewById(R.id.expandable_list_view);
        expandableListDetail = ExpandableTermDataRetrieve.getTermData();
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new ExpandableTermListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);

        //group click listener
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, final int position, long l) {

                    PopupMenu popup = new PopupMenu(TermListActivity.this, view);
                    popup.getMenuInflater().inflate(R.menu.popup_menu_termlist_term,
                            popup.getMenu());
                    popup.show();
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                         public boolean onMenuItemClick(MenuItem item) {
                            termListPos = position + 1;//position 0 is Term 1, correct todo term
                            switch (item.getItemId()) {
                                case R.id.action_term_details:
                                    TermViewActivity.color = true;
                                    Intent intent = new Intent(TermListActivity.this, TermViewActivity.class);
                                    startActivity(intent);
                                    return true;
                                case R.id.action_edit_term:
                                    TermViewActivity.color = false;
                                    fromTermList = true;
                                    TermListActivity.addNewTermFromTLAct = false;
                                    intent = new Intent(TermListActivity.this, TermEditActivity.class);
                                    startActivity(intent);
                                    finish();
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                return true;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Toast.makeText(
                        getApplicationContext(),
                        "No course actions available", Toast.LENGTH_SHORT
                ).show();
                return false;
            }
        });
        if (MainActivity.addNewTermFromMainAct || TermListActivity.addNewTermFromTLAct) {
            TermListActivity.addNewTermFromTLAct = false;
            MainActivity.addNewTermFromMainAct = false;
            Intent intent = new Intent(this, TermViewActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        expandableListAdapter = new ExpandableTermListAdapter(this, expandableListTitle, expandableListDetail);
        int count = expandableListAdapter.getGroupCount();
        for(int i = 0; i<count; ++i) {
            expandableListView.expandGroup(i);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.term_list_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_add_term:
                addNewTermFromTLAct = true;
                startActivity(new Intent(this,
                        TermEditActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
