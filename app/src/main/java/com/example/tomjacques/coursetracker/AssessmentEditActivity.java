package com.example.tomjacques.coursetracker;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class AssessmentEditActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
DatePickerDialog.OnDateSetListener{
    private DatabaseMgr db;
    private TextView dueDate;
    private TextView goalDate;
    private TextView assessAlert;
    private RadioButton assessSet;
    private RadioButton assessClear;
    private TextView courseNum;
    private TextView courseName;
    private EditText assessName;
    private EditText questions;
    private EditText cutScore;
    private EditText myScore;
    private String dateDue;
    private String dateGoal;
    private int goalAlert = 0;
    private int courseId;
    private int assessId;
    private static String[] assessTypeSpinner = {"Objective", "Performance"};
    private int assessTypeSpinnerPos;
    private int dayDueSpinnerPos;
    private int monthDueSpinnerPos;
    private int yearDueSpinnerPos;
    private int dayGoalSpinnerPos;
    private int monthGoalSpinnerPos;
    private int yearGoalSpinnerPos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_edit);
        dueDate = findViewById(R.id.assess_due_date);
        goalDate = findViewById(R.id.assess_goal_date);
        courseNum = findViewById(R.id.editAssess_courseNum);
        courseName = findViewById(R.id.editAssess_courseName);
        assessName = findViewById(R.id.editAssessName);
        questions = findViewById(R.id.editAssessQuestions);
        cutScore = findViewById(R.id.editAssess_cutScore);
        myScore = findViewById(R.id.editAssess_myScore);
        assessSet = findViewById(R.id.setAssessAlert);
        assessClear = findViewById(R.id.clearAssessAlert);

        Spinner spinner = (Spinner)findViewById(R.id.spinner_assessType);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item2, assessTypeSpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        if(!AssessmentListActivity.addAssessmentToList) {//assessment edit
            for (Assessment a : Assessment.getAssessmentList()) {
                if (AssessmentListAdapter.assessIdMatch == a.getAssessId()) {//match id from screen to list/db
                    assessName.setText(a.getAssessName());
                    questions.setText(Integer.toString(a.getQuestions()));
                    cutScore.setText(a.getPassFailCriteria());
                    myScore.setText(a.getMyScore());

                    dateDue = a.getDueDate();
                    dateGoal = a.getGoalDate();
                    assessAlert = findViewById(R.id.assess_goal);
                    if (a.getGoalDateAlert() == 1) {
                        assessAlert.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
                        assessSet.setChecked(true);
                    } else {
                        assessAlert.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        assessClear.setChecked(true);
                    }

                    String dD = dateDue.substring(5, 7) + "-" + dateDue.substring(8, 10) + "-" + dateDue.substring(0, 4);
                    String dG = dateGoal.substring(5, 7) + "-" + dateGoal.substring(8, 10) + "-"  + dateGoal.substring(0, 4);
                    //populate date spinners
                    dueDate.setText(dD);
                    goalDate.setText(dG);

                    assessId = a.getAssessId();
                    for (Course c : Course.getCourseList()) {
                        if(a.getCourseIdFK() == c.getCourseId()) {
                            courseNum.setText(c.getCourseNum());
                            courseName.setText(c.getName());
                            courseId = c.getCourseId();
                        }
                    }
                }
            }
        } else if (AssessmentListActivity.addAssessmentToList) {//assessment add
            for (Course c : Course.getCourseList()) {
                if(MainListAdapter.courseIdKeyMatch == c.getCourseId()) {
                    courseNum.setText(c.getCourseNum());
                    courseName.setText(c.getName());
                    courseId = c.getCourseId();
                    questions.setText(Integer.toString(0));
                    String dFormat = "mm-dd-yyyy";
                    dueDate.setText(dFormat);
                    goalDate.setText(dFormat);
                }
            }
        }
    }

    public void radioAssessClicked(View v) {
        boolean checked = ((RadioButton)v).isChecked();
        assessAlert = findViewById(R.id.assess_goal);
        switch(v.getId()) {
            case R.id.setAssessAlert:
                if(checked) {
                    assessAlert.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
                    goalAlert = 1;
                }
                break;

            case R.id.clearAssessAlert:
                if(checked) {
                    assessAlert.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    goalAlert = 0;
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.assessment_edit_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_assess:
                saveAssessment();
                return true;
            case R.id.action_cancel_assess:
                Intent intent;
                clearFields();
                if (AssessmentListActivity.addAssessmentToList) {
                    AssessmentListActivity.addAssessmentToList = false;
                    intent = new Intent(this, AssessmentListActivity.class);
                    startActivity(intent);
                }else {
                    intent = new Intent(this, AssessmentViewActivity.class);
                    startActivity(intent);
                }
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {

        switch (parent.getId()) {
            case R.id.spinner_assessType:
                assessTypeSpinnerPos = position;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void showAssessDueDatePicker(View v) {
        DatePickerFragment.datePicker = 3;
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.init(this,this);
        newFragment.show(getFragmentManager(), "assessDuePicker");
    }

    public void showAssessGoalDatePicker(View v) {
        DatePickerFragment.datePicker = 4;
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.init(this,this);
        newFragment.show(getFragmentManager(), "goalDatePicker");
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        month += 1;
        String dayString;
        String monthString;
        dayString = String.format("%02d", day);
        monthString = String.format("%02d", month);
        String datePickerString = monthString + "-" + dayString
                + "-" + Integer.toString(year);
        switch(DatePickerFragment.datePicker) {
            case 3:
                dueDate.setText(datePickerString);
                break;
            case 4:
                goalDate.setText(datePickerString);
                break;
        }
    }

    private void saveAssessment() {
        if (!assessName.getText().toString().equals("")) {
            String newDueDate  = dueDate.getText().toString().substring(6)
                    + "-" + dueDate.getText().toString().substring(0, 3)
                    + dueDate.getText().toString().substring(3, 5);
            String newGoalDate = goalDate.getText().toString().substring(6)
                    + "-" + goalDate.getText().toString().substring(0, 3)
                    + goalDate.getText().toString().substring(3, 5);
            db = new DatabaseMgr(this);
            Assessment assessment = new Assessment();
            try {
                assessment.setQuestions(Integer.parseInt(questions.getText().toString()));
                assessment.setAssessName(assessName.getText().toString());
                assessment.setAssessType(assessTypeSpinner[assessTypeSpinnerPos]);

                assessment.setPassFailCriteria(cutScore.getText().toString());
                assessment.setMyScore(myScore.getText().toString());
                assessment.setDueDate(newDueDate);
                assessment.setGoalDate(newGoalDate);
                assessment.setCourseIdFK(courseId);
                assessment.setAssessId(assessId);

                if (AssessmentListActivity.addAssessmentToList) {//new assessment
                    for (Course c : Course.getCourseList()) {
                        if (courseId == c.getCourseId()) {
                            c.getAssociatedAssessments().add(assessment);//add assessment to Course
                        }
                    }
                    Assessment.getAssessmentList().add(assessment);//add assessment to assessment list
                    db.insertAssessment(assessName.getText().toString(),
                            assessTypeSpinner[assessTypeSpinnerPos],
                            Integer.parseInt(questions.getText().toString()),
                            cutScore.getText().toString(),
                            myScore.getText().toString(),
                            newDueDate,
                            newGoalDate,
                            goalAlert,
                            courseId);
                    Assessment.getAssessmentList().clear();//clear assessment list
                    Assessment.setAssessmentList(db.getAssessmentListFromDB());//reload updated assessment list
                    AssessmentListAdapter.assessIdMatch = Assessment.getAssessmentList().get(Assessment.getAssessmentList().size() - 1).getAssessId();
                } else if (!AssessmentListActivity.addAssessmentToList) {//existing assessment
                    db.updateAssessment(assessId, assessName.getText().toString(),
                            assessTypeSpinner[assessTypeSpinnerPos],
                            Integer.parseInt(questions.getText().toString()),
                            cutScore.getText().toString(),
                            myScore.getText().toString(),
                            newDueDate,
                            newGoalDate,
                            goalAlert,
                            courseId);
                    Assessment.getAssessmentList().clear();//clear assessment list
                    Assessment.setAssessmentList(db.getAssessmentListFromDB());//reload updated assessment list
                }
                //todo listLoad method
                for (Course c : Course.getCourseList()) {//block to reload course assosciated assessments
                    c.getAssociatedAssessments().clear();
                    for (Assessment a : Assessment.getAssessmentList()) {
                        if (c.getCourseId() == a.getCourseIdFK()) {
                            c.getAssociatedAssessments().add(a);
                        }
                    }
                }
                AssessmentListActivity.addAssessmentToList = false;
                Toast.makeText(
                        getApplicationContext(),
                        "Assessment saved", Toast.LENGTH_SHORT
                ).show();
                Intent intent = new Intent(this, AssessmentViewActivity.class);
                startActivity(intent);
                finish();
            } catch (Exception e) {
                alertQuestions();
            }
        } else {
            AlertDialog.Builder ab = new AlertDialog.Builder(this);
            ab.setTitle("Missing Field")
                    .setMessage("Assess Name must be entered")
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(
                                        DialogInterface dialog,
                                        int yes) {
                                    //do nothing
                                    dialog.dismiss();
                                }
                            })
                    .create()
                    .show();
        }
    }

    private boolean alertQuestions() {
        AlertDialog.Builder ad;
        ad = new AlertDialog.Builder(this);
        ad.setTitle("Questions Field Issue")
                .setMessage("Question field can't be blank & must be a number")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(
                            DialogInterface dialog,
                            int ok) {
                        //do nothing
                        dialog.dismiss();
                    }

                })
                .create()
                .show();
        return true;
    }

    private void clearFields() {
        courseNum.setText("");
        courseName.setText("");
        assessName.setText("");
        questions.setText("");
        cutScore.setText("");
        myScore.setText("");
    }
}
