package com.example.tomjacques.coursetracker;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by tomjacques on 1/13/18.
 */

public class CourseViewActivity extends AppCompatActivity {
        public static boolean fromCourseView = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView dateStartText = findViewById(R.id.startDateTitle);
        TextView dateEndText = findViewById(R.id.endDateTitle);
        TextView courseNum = findViewById(R.id.courseNumber);
        TextView courseName = findViewById(R.id.courseName);
        TextView dateStart = findViewById(R.id.dateStart);
        TextView dateEnd = findViewById(R.id.dateEnd);
        TextView mentorName = findViewById(R.id.mentorName);
        TextView mentorEmail = findViewById(R.id.mentorEmail);
        TextView mentorPhone = findViewById(R.id.mentorPhone);
        TextView courseStatus = findViewById(R.id.courseStatus);
        TextView termNumber = findViewById(R.id.termNumber);

        //programmatic buttons with click listeners
        Button btnAssess = new Button(this);
        LinearLayout ll1 = findViewById(R.id.linear_assess);
        String assess = "Assessment";
        btnAssess.setText(assess);
        btnAssess.setLayoutParams(new LinearLayout.LayoutParams(370, 145));
        btnAssess.setBackgroundColor(getResources().getColor(R.color.lightBlue));
        ll1.addView(btnAssess);

        btnAssess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CourseViewActivity.this, AssessmentListActivity.class);
                startActivity(intent);
            }
        });

        Button btnNotes = new Button(this);
        LinearLayout ll2 = findViewById(R.id.linear_notes);
        String notes = "Notes";
        btnNotes.setText(notes);
        btnNotes.setLayoutParams(new LinearLayout.LayoutParams(370, 145));
        btnNotes.setBackgroundColor(getResources().getColor(R.color.lightBlue));
        ll2.addView(btnNotes);

        btnNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CourseViewActivity.this, NotesViewActivity.class);
                startActivity(intent);
            }
        });

        for (Course c : Course.getCourseList()) {
            if (MainListAdapter.courseIdKeyMatch == c.getCourseId()) {
                courseNum.setText(c.getCourseNum());
                courseName.setText(c.getName());
                if (c.getAlertStart() == 1) {
                    dateStartText.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
                } else {
                    dateStartText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
                if (c.getAlertEnd() == 1) {
                    dateEndText.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
                } else {
                    dateEndText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
                String reformattedDateStart = c.getDateStart();
                String reformattedDateEnd = c.getDateEnd();
                reformattedDateStart = reformattedDateStart.substring(5) + "-" + reformattedDateStart.substring(0, 4);
                reformattedDateEnd = reformattedDateEnd.substring(5) + "-" + reformattedDateEnd.substring(0, 4);
                dateStart.setText(reformattedDateStart);
                dateEnd.setText(reformattedDateEnd);

                courseStatus.setText(c.getStatus());
                if (c.getStatus().equals("Plan to Take")) {
                    courseStatus.setTextColor(Color.BLACK);
                }
                else if (c.getStatus().equals("In Progress")) {
                    courseStatus.setTextColor(Color.GREEN);
                }
                else if (c.getStatus().equals("Completed")) {
                    courseStatus.setTextColor(Color.BLUE);
                }
                else if (c.getStatus().equals("Dropped")) {
                    courseStatus.setTextColor(Color.RED);
                }

                for (Mentor m : Mentor.getMentorList()) {
                    if (m.getId() == c.getMentorIdFK()) {
                        mentorName.setText(m.getMentorName());
                        mentorEmail.setText(m.getMentorEmail());
                        mentorPhone.setText(m.getMentorPhone());
                    }
                }
                for (Term t : Term.getTermList()) {
                    String none = "None";
                    if (t.getTermId() == 1) {termNumber.setText(none);}
                    else if (t.getTermId() == c.getTermIdFK()) {
                        termNumber.setText(Integer.toString(t.getTermNumber()));
                    }
                }
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.course_view_actionbar, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                    intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                return true;
            case R.id.action_edit:
                fromCourseView = true;
                intent = new Intent(this, CourseEditActivity.class);
                startActivity(intent);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
